CC=gcc
CXX=g++
STD= -std=c++11
CFLAGS = -g -Wall

IDIR = -I/usr/local/include
IDIR += $(shell pkg-config --cflags opencv)

LIBS = -L/usr/local/lib -I/usr/local/lib/lib-ntl -lwebsockets -lpq -ljson-c
LIBS += $(shell pkg-config --libs --cflags opencv) -luuid

all: frc

frc: recognition.c recognition-client-function.cc recognition-client.c recognition-queue.c recognition-face-enrollment.cc recognition-face-verification.cc recognition-bri-api.cc
	$(CXX) $(STD) $(CFLAGS) -o frc recognition.c recognition-client-function.cc recognition-client.c recognition-queue.c recognition-face-enrollment.cc recognition-face-verification.cc recognition-bri-api.cc $(IDIR) $(LIBS)

clean:
	rm frc
