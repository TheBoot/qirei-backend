--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.8
-- Dumped by pg_dump version 9.5.8

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: buyer_details; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE buyer_details (
    id integer NOT NULL,
    id_user integer,
    account_no character varying(128),
    created_by character varying(128),
    created_time timestamp without time zone DEFAULT now()
);


ALTER TABLE buyer_details OWNER TO root;

--
-- Name: buyer_details_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE buyer_details_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE buyer_details_id_seq OWNER TO root;

--
-- Name: buyer_details_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE buyer_details_id_seq OWNED BY buyer_details.id;


--
-- Name: enrolled_faces; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE enrolled_faces (
    id integer NOT NULL,
    id_user integer,
    path character varying,
    created_by character varying(128),
    created_time timestamp without time zone DEFAULT now(),
    label integer
);


ALTER TABLE enrolled_faces OWNER TO root;

--
-- Name: enrolled_faces_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE enrolled_faces_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE enrolled_faces_id_seq OWNER TO root;

--
-- Name: enrolled_faces_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE enrolled_faces_id_seq OWNED BY enrolled_faces.id;


--
-- Name: qrcodes; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE qrcodes (
    id integer NOT NULL,
    seller character varying(128),
    created_time timestamp without time zone DEFAULT now(),
    sell_value numeric,
    created_by character varying(128),
    uuid character varying(37)
);


ALTER TABLE qrcodes OWNER TO root;

--
-- Name: qrcodes_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE qrcodes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE qrcodes_id_seq OWNER TO root;

--
-- Name: qrcodes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE qrcodes_id_seq OWNED BY qrcodes.id;


--
-- Name: seller_details; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE seller_details (
    id integer NOT NULL,
    id_user integer,
    company_code character varying(3),
    account_no character varying(128),
    created_by character varying(128),
    created_time timestamp without time zone DEFAULT now()
);


ALTER TABLE seller_details OWNER TO root;

--
-- Name: seller_details_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE seller_details_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE seller_details_id_seq OWNER TO root;

--
-- Name: seller_details_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE seller_details_id_seq OWNED BY seller_details.id;


--
-- Name: transactions; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE transactions (
    id integer NOT NULL,
    seller_id integer,
    buyer_id integer,
    referral_no character varying(128),
    value integer,
    created_by character varying(128),
    created_time timestamp without time zone DEFAULT now()
);


ALTER TABLE transactions OWNER TO root;

--
-- Name: transactions_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE transactions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE transactions_id_seq OWNER TO root;

--
-- Name: transactions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE transactions_id_seq OWNED BY transactions.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE users (
    id integer NOT NULL,
    name character varying(128),
    password character varying,
    type character varying(10),
    bank_client_id character varying(64),
    bank_client_secret character varying(64),
    created_by character varying(128),
    created_time timestamp without time zone DEFAULT now(),
    enrolled boolean
);


ALTER TABLE users OWNER TO root;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE users_id_seq OWNER TO root;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE users_id_seq OWNED BY users.id;


--
-- Name: validated; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE validated (
    id integer NOT NULL,
    qrcodes_id integer,
    buyer character varying(128),
    qr boolean,
    face boolean,
    status character varying(128),
    created_by character varying(128),
    created_time timestamp without time zone DEFAULT now()
);


ALTER TABLE validated OWNER TO root;

--
-- Name: validated_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE validated_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE validated_id_seq OWNER TO root;

--
-- Name: validated_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE validated_id_seq OWNED BY validated.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY buyer_details ALTER COLUMN id SET DEFAULT nextval('buyer_details_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY enrolled_faces ALTER COLUMN id SET DEFAULT nextval('enrolled_faces_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY qrcodes ALTER COLUMN id SET DEFAULT nextval('qrcodes_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY seller_details ALTER COLUMN id SET DEFAULT nextval('seller_details_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY transactions ALTER COLUMN id SET DEFAULT nextval('transactions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY validated ALTER COLUMN id SET DEFAULT nextval('validated_id_seq'::regclass);


--
-- Data for Name: buyer_details; Type: TABLE DATA; Schema: public; Owner: root
--

COPY buyer_details (id, id_user, account_no, created_by, created_time) FROM stdin;
1	2	032901012345501	buyer	2017-09-14 13:15:47.76887
\.


--
-- Name: buyer_details_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('buyer_details_id_seq', 1, true);


--
-- Data for Name: enrolled_faces; Type: TABLE DATA; Schema: public; Owner: root
--

COPY enrolled_faces (id, id_user, path, created_by, created_time, label) FROM stdin;
625	1	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s1/efd234f4-4731-2017-30b2-0913993a0862.png	cashier01	2017-09-18 14:29:59.762293	5
626	1	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s1/74618f01-6753-0fe9-600a-10ca6a87c793.png	cashier01	2017-09-18 14:30:03.883751	5
627	1	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s1/1d2e59ba-eabd-2afe-5579-592af8fe5888.png	cashier01	2017-09-18 14:30:07.714933	5
628	1	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s1/1a46c6b1-13df-1f62-ced2-7c1558f47152.png	cashier01	2017-09-18 14:30:11.513299	5
629	1	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s1/9113ba81-ad1b-d416-6517-7862b7367d96.png	cashier01	2017-09-18 14:30:14.926914	5
630	1	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s1/4509a558-8250-0cbb-a653-13d3c52d9952.png	cashier01	2017-09-18 14:30:18.834008	5
631	1	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s1/94690f2a-11f5-8ad4-6b87-f8df9d333df3.png	cashier01	2017-09-18 14:30:22.669494	5
632	1	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s1/99fc6c91-059c-dab7-1ae1-3cb5f262b85d.png	cashier01	2017-09-18 14:30:26.969882	5
633	1	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s1/abc4dde8-f5fd-9f0f-8a39-3da60fca10d3.png	cashier01	2017-09-18 14:30:30.951888	5
675	4	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s4/e6858b51-2e3b-0e89-7f54-0090956d63e7.png	satria	2017-09-18 15:09:07.018928	4
676	4	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s4/4d85e016-6567-be35-55f8-328ab13a3f93.png	satria	2017-09-18 15:09:10.03823	4
677	4	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s4/db7e7f55-9cee-7e51-ff4d-c7b9c1482dab.png	satria	2017-09-18 15:09:13.901927	4
577	4	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s4/be0d8182-fba0-4232-d0ec-075f21f21205.png	satria	2017-09-18 14:19:52.563898	4
578	4	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s4/a46d1f20-8d1c-a03e-3fd0-236c1adf21d9.png	satria	2017-09-18 14:19:56.016368	4
678	4	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s4/e2ac46db-d959-f924-ad7c-5eac594be755.png	satria	2017-09-18 15:09:17.906349	4
679	4	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s4/8196fc6d-f794-cd30-d6c0-00b6dbca8cb7.png	satria	2017-09-18 15:09:22.018465	4
680	4	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s4/74d1763f-3ce6-33ee-a15a-e610893a63f2.png	satria	2017-09-18 15:09:25.361913	4
681	4	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s4/c954cd17-99b5-d0d7-a563-bcb5fab03ab3.png	satria	2017-09-18 15:09:28.977911	4
682	4	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s4/26b2b206-d94e-3c72-2881-ec1b97528466.png	satria	2017-09-18 15:09:32.530256	4
580	4	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s4/5ada21e5-3da7-7eba-5904-8e776f04dd5c.png	satria	2017-09-18 14:20:01.93945	4
581	4	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s4/6479aa3d-2fd5-d507-d49e-1df27fb5d0ca.png	satria	2017-09-18 14:20:05.499824	4
582	4	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s4/5cc11f2e-4496-cc17-80fe-35b4003c29f0.png	satria	2017-09-18 14:20:08.496335	4
583	4	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s4/bca0a9ed-1e2d-2ba0-bcbd-dda907563682.png	satria	2017-09-18 14:20:11.713715	4
584	4	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s4/c5744146-4f0b-23f7-e5a1-cdd08d782525.png	satria	2017-09-18 14:20:14.722287	4
585	4	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s4/362c1c24-1139-a943-6f46-baff5cdd0d2e.png	satria	2017-09-18 14:20:26.880657	4
586	4	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s4/1769375f-d9c3-9f4b-9889-081ff20d1e0e.png	satria	2017-09-18 14:20:30.137723	4
587	4	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s4/2a5d734d-040e-7fa5-362f-c4fa761f9cd6.png	satria	2017-09-18 14:20:33.44271	4
588	4	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s4/bd34f70b-e102-c154-3166-b6a3a4b6cd73.png	satria	2017-09-18 14:20:36.826986	4
589	4	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s4/0bcc3409-b692-f40a-5028-2b688ba30012.png	satria	2017-09-18 14:20:39.769398	4
634	1	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s1/22db7705-61a8-ecad-c28d-f7de20200076.png	cashier01	2017-09-18 14:30:34.564599	5
635	1	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s1/744c876b-dffa-0186-cb9d-f15c3f87944f.png	cashier01	2017-09-18 14:30:38.126558	5
645	2	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s2/e17a4a5a-eaa2-b406-5cf6-ccdc9f251018.png	ica	2017-09-18 15:05:13.402031	1
646	2	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s2/262ef705-e87f-80a6-813c-a908079da18b.png	ica	2017-09-18 15:05:17.415381	1
647	2	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s2/9e802f2b-a5b3-6dd7-066c-b3ee867a5f1c.png	ica	2017-09-18 15:05:21.248036	1
648	2	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s2/8791a7e8-d130-9d01-3834-0f532bdeae19.png	ica	2017-09-18 15:05:25.386115	1
649	2	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s2/4e58e6a7-7f44-5a7f-6d3d-8d95b8e1bacb.png	ica	2017-09-18 15:05:29.66446	1
650	2	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s2/129a35d9-e2e4-7f9b-fca6-e3d87d1dcf3a.png	ica	2017-09-18 15:05:33.584655	1
651	2	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s2/fa856605-872c-c4d1-ac3b-ba26e172c3b1.png	ica	2017-09-18 15:05:37.667932	1
652	2	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s2/a2583592-494f-0385-695c-3dd3fd7e6d4a.png	ica	2017-09-18 15:05:39.658302	1
653	2	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s2/aa65a48b-b5e8-027d-498b-5770320e314d.png	ica	2017-09-18 15:05:44.566221	1
654	2	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s2/c9b68645-8b62-389b-fd6f-bb700f9b49d9.png	ica	2017-09-18 15:05:48.406475	1
655	2	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s2/7f23bb39-eb65-c77b-646c-1fafade5572b.png	ica	2017-09-18 15:05:52.579105	1
656	2	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s2/78089e87-1192-d1cb-4d74-3bce74d856db.png	ica	2017-09-18 15:05:55.321306	1
657	2	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s2/d2b8986d-b0ae-aadd-3b47-2394376fa750.png	ica	2017-09-18 15:06:00.014956	1
658	2	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s2/eb106b07-d3b9-df01-15d7-7deecbeb6933.png	ica	2017-09-18 15:06:03.217388	1
659	2	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s2/5b071699-42d4-7aaa-5002-60f9e15e3d35.png	ica	2017-09-18 15:06:06.790073	1
660	2	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s2/dec561b6-5179-a06d-a8a4-800916d72a65.png	ica	2017-09-18 15:06:11.007403	1
661	2	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s2/0da409bb-c00b-f21a-8272-bd1d48f5d9b3.png	ica	2017-09-18 15:06:15.198658	1
662	2	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s2/9425eeaf-93db-34c7-ce4f-61e446ec8e2d.png	ica	2017-09-18 15:06:19.556508	1
663	2	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s2/6e78e364-6cb9-b9f3-4951-015d2255ac8c.png	ica	2017-09-18 15:06:22.802971	1
636	1	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s1/7b8fa39a-32a8-7cfe-905e-08c7aaabec9d.png	cashier01	2017-09-18 14:30:42.190664	5
637	1	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s1/313fb296-080c-928d-fe24-e365761bc481.png	cashier01	2017-09-18 14:30:45.996267	5
638	1	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s1/e70b45d9-4f5a-6248-f66b-59fb0f70a544.png	cashier01	2017-09-18 14:30:50.084205	5
639	1	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s1/a933f01c-89bd-a97f-d1be-271e5dfc0897.png	cashier01	2017-09-18 14:30:53.550451	5
640	1	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s1/8beb810b-ad3a-2287-0c41-f006038247f9.png	cashier01	2017-09-18 14:30:57.4957	5
641	1	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s1/41931764-0c66-b03d-ae8e-a81840a15d5c.png	cashier01	2017-09-18 14:31:01.273515	5
642	1	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s1/11e95666-a9c6-dd71-dd8f-e0bfd933e4e2.png	cashier01	2017-09-18 14:31:05.189744	5
643	1	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s1/24010fb5-e240-f6c2-38de-4a6573b774b1.png	cashier01	2017-09-18 14:31:08.394874	5
644	1	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s1/1bb373b0-05fd-fa2d-0b12-a57286380c91.png	cashier01	2017-09-18 14:31:12.62458	5
685	4	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s4/13565637-c60c-0e44-4dd3-d69d1a8058c5.png	satria	2017-09-18 15:40:58.273426	4
686	4	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s4/b42e6805-ec7f-6a55-c7f9-6fec7f9b8909.png	satria	2017-09-18 15:41:02.495103	4
687	4	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s4/8d750b6e-f2e0-1757-fb51-ccee05f6d1c1.png	satria	2017-09-18 15:41:06.427047	4
688	4	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s4/9699a61f-3725-8ec5-e004-0b92a7d74b65.png	satria	2017-09-18 15:41:10.54446	4
689	4	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s4/feebd561-69d1-74ae-e96e-496fe9e26d11.png	satria	2017-09-18 15:41:14.219995	4
572	4	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s4/c8042ee7-97fc-01e7-a9df-a0b519633195.png	satria	2017-09-18 14:19:36.24187	4
573	4	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s4/e2a363ba-9d39-b604-1b92-7584b48bf081.png	satria	2017-09-18 14:19:39.64212	4
574	4	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s4/d4ec8e99-8f2a-d785-684f-8aefd2f2b1e2.png	satria	2017-09-18 14:19:43.106506	4
575	4	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s4/a11f53e1-db55-e156-ecb5-ac0802a80e73.png	satria	2017-09-18 14:19:46.152246	4
576	4	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s4/c67d954f-e788-5faa-63f0-36ffd8615450.png	satria	2017-09-18 14:19:49.342518	4
665	4	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s4/a7090a46-4c27-9f50-2b47-4122903bc646.png	satria	2017-09-18 15:08:26.199156	4
666	4	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s4/116b1384-1d7f-dede-0d5f-7167fd4064ec.png	satria	2017-09-18 15:08:30.258156	4
667	4	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s4/c67f621f-6868-532f-15c6-eed83c1a4b7f.png	satria	2017-09-18 15:08:34.17166	4
668	4	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s4/b1c55b3b-fcb4-c918-298f-acc8a0f5f377.png	satria	2017-09-18 15:08:38.538159	4
669	4	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s4/bafb7c64-7aa4-f2e5-1ddc-25dad2b19063.png	satria	2017-09-18 15:08:42.565033	4
664	2	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s2/91c9fe31-0ca6-c0ff-7373-6fb0ef0f4747.png	ica	2017-09-18 15:06:27.191809	1
715	5	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s5/17303e56-70db-ddad-bea7-be317250ef0b.png	alhakam	2017-09-18 19:31:30.436084	3
716	5	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s5/a785aa80-47ab-bf2d-7839-be855abafe14.png	alhakam	2017-09-18 19:31:33.606647	3
717	5	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s5/e3529c1a-2507-f73c-18fb-d03f2c9e06fd.png	alhakam	2017-09-18 19:31:37.58651	3
718	5	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s5/0e989c6a-6485-a6a1-42b9-ea61f89d4480.png	alhakam	2017-09-18 19:31:41.294189	3
719	5	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s5/961b8416-bda2-8290-5f49-fe4793c2182f.png	alhakam	2017-09-18 19:31:44.662763	3
720	5	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s5/0830aef9-06b7-15be-ed11-1496665e9753.png	alhakam	2017-09-18 19:31:47.99487	3
721	5	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s5/ee153a36-564f-65c3-0dc8-9370af304ca1.png	alhakam	2017-09-18 19:31:50.922046	3
722	5	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s5/0bdd7e9f-ecd3-6c57-f981-45f63f7ba55c.png	alhakam	2017-09-18 19:31:54.960948	3
723	5	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s5/4498e948-cc1c-90c2-50bc-e1bb3f4f4752.png	alhakam	2017-09-18 19:31:56.854638	3
724	5	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s5/eca33fb5-e991-5535-0555-824a3851a4f0.png	alhakam	2017-09-18 19:32:01.10438	3
725	5	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s5/eb8dd10b-6ff4-1a85-63b0-3052d12206b9.png	alhakam	2017-09-18 19:32:04.879048	3
726	5	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s5/aa8623fc-540c-95e9-be10-b265b79ee113.png	alhakam	2017-09-18 19:32:08.732885	3
727	5	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s5/b6b77acb-bc7e-f53a-6090-a45f41d74e57.png	alhakam	2017-09-18 19:32:12.856964	3
728	5	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s5/8ba96451-9370-57d3-6801-d5fc03b814bb.png	alhakam	2017-09-18 19:32:16.690874	3
729	5	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s5/03ce9f91-4d1c-62dd-68cb-7fec4cb24449.png	alhakam	2017-09-18 19:32:20.364108	3
730	5	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s5/940169b2-aee3-9abe-1eeb-b75bfa151f2a.png	alhakam	2017-09-18 19:32:24.167323	3
731	5	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s5/3e9c6024-807b-8373-0aae-ff8c57bace58.png	alhakam	2017-09-18 19:32:27.263322	3
732	5	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s5/3dccdc3c-7296-8864-4f23-9a6fde209350.png	alhakam	2017-09-18 19:32:30.767496	3
733	5	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s5/cd2258ca-b341-bbb0-f08c-81666fd515bb.png	alhakam	2017-09-18 19:32:34.424312	3
734	5	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s5/f52b3539-39be-72b3-a84a-0aca86a21c4a.png	alhakam	2017-09-18 19:32:38.129186	3
590	4	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s4/56b36eb8-4246-0f9d-f0e4-7ee44befe0e2.png	satria	2017-09-18 14:20:42.9592	4
591	4	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s4/92db8fc8-10ba-4ebc-7c0d-7255c86e2159.png	satria	2017-09-18 14:20:46.11393	4
592	4	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s4/c37d8b64-68d0-9b76-e01c-67d030d0670e.png	satria	2017-09-18 14:20:49.099381	4
593	4	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s4/1a453a19-fc47-7557-8696-fbde6d5129d0.png	satria	2017-09-18 14:20:52.14074	4
594	4	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s4/ee2a5c73-c0fd-3c17-e2a9-b0d96436f512.png	satria	2017-09-18 14:20:54.905299	4
595	4	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s4/289fabae-aef0-5c88-3694-12859c3628a9.png	satria	2017-09-18 14:20:58.149376	4
596	4	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s4/df8cde50-257c-6490-44bb-789370753335.png	satria	2017-09-18 14:21:01.159798	4
597	4	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s4/cf0b3a35-85bc-fd71-bf2f-332719e74d3b.png	satria	2017-09-18 14:21:03.554361	4
579	4	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s4/2fbe471a-645a-7e75-6e51-bc4a0da3a381.png	satria	2017-09-18 14:19:58.827623	4
598	4	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s4/40161801-1f32-01d3-b9c8-bb46db48c991.png	satria	2017-09-18 14:21:06.803526	4
599	4	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s4/d7a806a4-dbbc-7546-0c42-20624df33366.png	satria	2017-09-18 14:21:09.81481	4
600	4	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s4/af3da73f-766b-b8da-e0e4-136bafb87baf.png	satria	2017-09-18 14:21:13.102474	4
601	4	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s4/99a3a778-8951-7746-8bd6-5e5d70b50758.png	satria	2017-09-18 14:21:16.269918	4
602	4	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s4/9a5ad056-d44e-b2d9-4827-2fc9554be85d.png	satria	2017-09-18 14:21:19.326595	4
603	4	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s4/8a27f4b5-b8f4-d9bc-dcd2-68df12822f83.png	satria	2017-09-18 14:21:22.106108	4
604	4	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s4/919bfbbd-ed9e-cf8c-0f78-efbbdf0c5d97.png	satria	2017-09-18 14:21:25.747423	4
605	4	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s4/093a3dc7-ff4a-8ef8-cbaa-c0f4822bbd72.png	satria	2017-09-18 14:22:49.845446	4
606	4	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s4/b90a686f-7de3-099d-94f8-a3b13a83dea7.png	satria	2017-09-18 14:22:52.673415	4
607	4	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s4/18a7f553-088a-ef88-d285-5fb730c8ee77.png	satria	2017-09-18 14:22:56.16903	4
608	4	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s4/3a9d1d32-18cb-84d4-c927-81bbba9857b5.png	satria	2017-09-18 14:22:59.540164	4
609	4	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s4/f4bdd861-8e79-1efb-147f-b078d4e83832.png	satria	2017-09-18 14:23:02.588303	4
610	4	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s4/159cb485-f5c3-c675-e51d-0a15e1d4654d.png	satria	2017-09-18 14:23:05.292761	4
611	4	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s4/c0a81929-39e4-8013-e0f5-a8fb3ac9ae5c.png	satria	2017-09-18 14:23:08.17101	4
612	4	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s4/981688a9-3873-c602-f571-9b823a23f124.png	satria	2017-09-18 14:23:11.676532	4
690	4	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s4/4d2a631f-b4c2-7c64-5a36-ed48627689f9.png	satria	2017-09-18 15:41:17.138902	4
691	4	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s4/80268b0a-fa39-d172-c12e-26f8afac98dc.png	satria	2017-09-18 15:41:21.648722	4
692	4	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s4/c7ab1bb6-95cb-0d91-0520-dbeddafedb80.png	satria	2017-09-18 15:41:25.208679	4
693	4	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s4/2a4d3ca3-97a4-2f8d-a84b-1fe0bf297bfc.png	satria	2017-09-18 15:41:28.718501	4
694	4	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s4/1aff9993-dc83-eb51-ff90-1cda7a988007.png	satria	2017-09-18 15:41:33.193423	4
695	4	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s4/562c0046-791b-b74d-c77f-0626cc9a4846.png	satria	2017-09-18 15:41:37.694022	4
696	4	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s4/767a6e0b-90ca-5459-396b-351710318303.png	satria	2017-09-18 15:41:40.857132	4
697	4	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s4/aab5367e-e405-1a31-fa6e-099be6c1a38a.png	satria	2017-09-18 15:41:44.977346	4
698	4	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s4/c11855de-b3dc-ef2f-b682-faa29c7f043f.png	satria	2017-09-18 15:41:49.32578	4
699	4	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s4/fcbca7db-3d36-080c-1594-6b9c9ad10552.png	satria	2017-09-18 15:41:53.585967	4
700	4	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s4/833132ca-d1be-ad8b-5e8d-00e25e8667e6.png	satria	2017-09-18 15:41:57.150637	4
701	4	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s4/e0ce8f5b-29ba-9f22-c171-64433830d035.png	satria	2017-09-18 15:42:00.987805	4
702	4	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s4/912fc801-043b-66c0-8f7c-c93dad37a0ed.png	satria	2017-09-18 15:42:05.290966	4
703	4	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s4/7912704d-c15e-6359-e1c1-4a897f8bbe68.png	satria	2017-09-18 15:42:09.441259	4
704	4	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s4/c7af2ef5-6781-4032-3408-6f7d08f9ad90.png	satria	2017-09-18 15:42:13.851638	4
670	4	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s4/9b9d3dc3-d620-f862-7f3c-a54afcd3fd58.png	satria	2017-09-18 15:08:46.836696	4
671	4	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s4/338b81c7-6ea1-cb70-11d8-8995c88d2d36.png	satria	2017-09-18 15:08:50.315483	4
672	4	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s4/59ddd84e-c1c2-f796-4254-7756e8f3b0b7.png	satria	2017-09-18 15:08:54.074683	4
673	4	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s4/5ba901f0-7c48-e4b5-75c7-32581aeccdcd.png	satria	2017-09-18 15:08:58.267888	4
674	4	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s4/f7897139-5d9c-017e-023a-ba723db9c497.png	satria	2017-09-18 15:09:02.676064	4
613	4	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s4/d495a622-72a4-2025-b0f6-2447b08f13a4.png	satria	2017-09-18 14:23:15.042089	4
614	4	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s4/a5471d5a-0ed7-ac67-efa9-b919cb72cf93.png	satria	2017-09-18 14:23:17.965017	4
615	4	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s4/596b1929-84ac-e3f6-28ea-07946054fcb7.png	satria	2017-09-18 14:23:21.45602	4
616	4	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s4/eba1ff4a-a129-ac3c-ba80-a244a242d976.png	satria	2017-09-18 14:23:25.262731	4
617	4	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s4/f50ba588-a655-981d-176f-ee41e8d441c8.png	satria	2017-09-18 14:23:28.426161	4
618	4	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s4/87907584-a4a2-64a4-4133-031e243969c9.png	satria	2017-09-18 14:23:31.722915	4
619	4	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s4/756e530a-3c83-77db-f93d-5768beb479fa.png	satria	2017-09-18 14:23:35.593288	4
620	4	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s4/f63744f0-fbd7-ae18-3133-07c42b1fe0e0.png	satria	2017-09-18 14:23:38.20907	4
621	4	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s4/23cf9e21-7674-98b0-e6f0-82e79be1b19d.png	satria	2017-09-18 14:23:41.692191	4
622	4	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s4/d3f4861d-d53d-cc53-794d-5bdf402ac6c5.png	satria	2017-09-18 14:23:44.640888	4
623	4	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s4/1c23f7c9-3d5a-9c8a-e6ab-422458d36c14.png	satria	2017-09-18 14:23:47.460228	4
624	4	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s4/083a6d2a-1ca7-86ba-f2d9-6b13e2f2eb99.png	satria	2017-09-18 14:23:51.205153	4
683	4	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s4/9beb9ff2-3a8c-56e9-0805-46338d77aa24.png	satria	2017-09-18 15:09:36.670269	4
684	4	/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/s4/7bc61bf1-35a0-3549-7824-f166283b11b8.png	satria	2017-09-18 15:09:40.432406	4
\.


--
-- Name: enrolled_faces_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('enrolled_faces_id_seq', 734, true);


--
-- Data for Name: qrcodes; Type: TABLE DATA; Schema: public; Owner: root
--

COPY qrcodes (id, seller, created_time, sell_value, created_by, uuid) FROM stdin;
1	cashier01	2017-09-12 14:53:19.526057	50000	50000	714fa8a4-978f-11e7-98a6-080027b7e475
2	cashier01	2017-09-12 14:56:49.386953	50000	cashier01	ee65ef38-978f-11e7-98a6-080027b7e475
3	cashier01	2017-09-12 15:03:01.313424	50000	cashier01	cc1574b6-9790-11e7-98a6-080027b7e475
4	cashier01	2017-09-12 15:04:01.921517	50000	cashier01	f03584d0-9790-11e7-98a6-080027b7e475
5	cashier01	2017-09-12 15:05:56.191812	50000	cashier01	3451bc10-9791-11e7-98a6-080027b7e475
6	cashier01	2017-09-12 15:06:54.270215	50000	cashier01	56efd806-9791-11e7-98a6-080027b7e475
7	cashier01	2017-09-12 15:08:39.484604	50000	cashier01	95a64972-9791-11e7-98a6-080027b7e475
8	cashier01	2017-09-12 15:43:22.127468	50000	cashier01	6f004ebc-9796-11e7-98a6-080027b7e475
9	cashier01	2017-09-12 15:53:10.63539	50000	cashier01	cdc78220-9797-11e7-98a6-080027b7e475
10	2#100	2017-09-12 17:08:41.313087	50000	2#100	983#100101170912#100
11	cashier01	2017-09-12 17:12:39.231392	50000	cashier01	0101170912
12	cashier01	2017-09-12 17:14:13.905471	50000	cashier01	0101170912
13	cashier01	2017-09-12 17:17:16.937676	50000	cashier01	101170912
14	cashier01	2017-09-12 17:20:08.619668	50000	cashier01	101170912
15	cashier01	2017-09-12 17:20:52.427133	50000	cashier01	101170912
16	810	2017-09-12 17:21:38.878108	50000	810	683810101170912810
17	f10	2017-09-12 17:26:22.408775	50000	f10	983f10101170912f10
18	k10	2017-09-12 17:28:36.702251	50000	k10	291k10101170912k10
19	100	2017-09-12 17:30:18.89667	50000	100	402100101170912100
20		2017-09-13 08:13:22.761784	50000		208101170913100
21		2017-09-13 08:31:22.010287	50000		446101170913100
22		2017-09-13 08:31:44.800067	50000		301101170913100
23		2017-09-13 08:31:44.927105	50000		301101170913100
24		2017-09-13 08:33:40.8357	50000		167101170913100
25		2017-09-13 08:34:59.182652	50000		668101170913100
26		2017-09-13 09:13:33.907309	50000		869101170913100
27		2017-09-13 09:42:51.467193	50000		348101170913100
28	cashier01	2017-09-15 08:09:12.739683	50000	cashier01	964101170915100
29	cashier01	2017-09-15 16:58:43.159483	50000	cashier01	426101170915100
30	cashier01	2017-09-15 17:04:04.34993	50000	cashier01	113101170915100
31	cashier01	2017-09-18 09:22:02.055995	50000	cashier01	158101170918100
32	cashier01	2017-09-18 10:27:07.280065	50000	cashier01	987101170918100
33	cashier01	2017-09-18 10:29:35.994058	50000	cashier01	154101170918100
34	cashier01	2017-09-18 10:35:55.581539	50000	cashier01	273101170918100
35	cashier01	2017-09-18 12:58:32.992264	50000	cashier01	352101170918100
36	cashier01	2017-09-18 13:36:57.652604	50000	cashier01	113101170918100
37	cashier01	2017-09-18 13:44:41.755213	50000	cashier01	674101170918100
38	cashier01	2017-09-18 13:49:00.558854	50000	cashier01	419101170918100
39	cashier01	2017-09-18 14:33:02.610554	50000	cashier01	224101170918100
40	cashier01	2017-09-18 14:33:31.714728	50000	cashier01	452101170918100
41	cashier01	2017-09-18 14:34:33.458381	50000	cashier01	890101170918100
42	cashier01	2017-09-18 14:34:41.562795	50000	cashier01	168101170918100
43	cashier01	2017-09-18 14:34:41.689953	50000	cashier01	168101170918100
44	cashier01	2017-09-18 14:34:59.922598	50000	cashier01	547101170918100
45	cashier01	2017-09-18 14:35:25.72015	50000	cashier01	338101170918100
46	cashier01	2017-09-18 15:10:06.657957	50000	cashier01	790101170918100
47	cashier01	2017-09-18 15:11:32.003102	50000	cashier01	516101170918100
48	cashier01	2017-09-18 15:11:32.604949	50000	cashier01	906101170918100
49	cashier01	2017-09-18 15:11:33.706969	50000	cashier01	410101170918100
50	cashier01	2017-09-18 15:11:36.294067	50000	cashier01	506101170918100
51	cashier01	2017-09-18 15:11:36.494254	50000	cashier01	506101170918100
52	cashier01	2017-09-18 15:11:36.592689	50000	cashier01	506101170918100
53	cashier01	2017-09-18 15:11:45.740564	50000	cashier01	677101170918100
54	cashier01	2017-09-18 15:12:01.462915	50000	cashier01	229101170918100
55	cashier01	2017-09-18 15:12:06.1122	50000	cashier01	821101170918100
56	cashier01	2017-09-18 15:12:06.388817	50000	cashier01	821101170918100
57	cashier01	2017-09-18 15:12:23.75579	50000	cashier01	986101170918100
58	cashier01	2017-09-18 15:12:28.284087	50000	cashier01	128101170918100
59	cashier01	2017-09-18 15:12:29.101128	50000	cashier01	577101170918100
60	cashier01	2017-09-18 15:14:44.12857	50000	cashier01	769101170918100
61	cashier01	2017-09-18 15:31:30.874744	50000	cashier01	842101170918100
62	cashier01	2017-09-18 15:42:47.887379	50000	cashier01	284101170918100
63	cashier01	2017-09-18 16:13:30.920746	50000	cashier01	868101170918100
64	cashier01	2017-09-18 16:22:35.117859	50000	cashier01	682101170918100
65	cashier01	2017-09-18 16:22:35.612988	50000	cashier01	682101170918100
66	cashier01	2017-09-18 16:22:50.223151	50000	cashier01	782101170918100
67	cashier01	2017-09-18 17:43:04.168166	10000	cashier01	564101170918100
68	cashier01	2017-09-18 18:57:49.498361	10000	cashier01	908101170918020
69	cashier01	2017-09-18 19:37:03.593357	10000	cashier01	279101170918020
\.


--
-- Name: qrcodes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('qrcodes_id_seq', 69, true);


--
-- Data for Name: seller_details; Type: TABLE DATA; Schema: public; Owner: root
--

COPY seller_details (id, id_user, company_code, account_no, created_by, created_time) FROM stdin;
1	1	101	020601121234503	admin	2017-09-12 16:06:36.351945
\.


--
-- Name: seller_details_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('seller_details_id_seq', 1, true);


--
-- Data for Name: transactions; Type: TABLE DATA; Schema: public; Owner: root
--

COPY transactions (id, seller_id, buyer_id, referral_no, value, created_by, created_time) FROM stdin;
\.


--
-- Name: transactions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('transactions_id_seq', 1, false);


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: root
--

COPY users (id, name, password, type, bank_client_id, bank_client_secret, created_by, created_time, enrolled) FROM stdin;
1	cashier01	5e0fb950a170f26f6a071be306cb48ab	seller	\N	\N	\N	2017-09-15 11:16:21.67784	t
3	preste	28bc8a44625566ad3dc1b0ce6ee1b561	buyer	\N	\N	\N	2017-09-15 16:18:03.454919	f
2	ica	1e628956ae1080b8caf3d79545a2d0a7	buyer	\N	\N	\N	2017-09-15 11:16:21.67784	t
5	alhakam	d2e9414bfc43528e25e4013e2421f8d4	buyer	\N	\N	\N	2017-09-18 19:29:50.578584	t
4	satria	e00b29d5b34c3f78df09d45921c9ec47	buyer	\N	\N	\N	2017-09-18 14:18:46.845121	f
\.


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('users_id_seq', 5, true);


--
-- Data for Name: validated; Type: TABLE DATA; Schema: public; Owner: root
--

COPY validated (id, qrcodes_id, buyer, qr, face, status, created_by, created_time) FROM stdin;
2	28	ica	t	\N	\N	\N	2017-09-15 08:40:12.809904
20	28	ica	t	f	qr-verified	ica	2017-09-15 16:16:56.065456
1	28	ica	t	f	face-verification	ica	2017-09-15 08:12:22.866604
3	28	ica	t	f	qr-verified	ica	2017-09-15 08:57:51.698642
4	28	ica	t	f	qr-verified	ica	2017-09-15 08:59:24.881514
5	28	ica	t	f	qr-verified	ica	2017-09-15 13:28:51.195152
6	28	ica	t	f	qr-verified	ica	2017-09-15 13:31:05.180159
7	28	ica	t	f	qr-verified	ica	2017-09-15 13:53:21.871832
8	28	ica	t	f	qr-verified	ica	2017-09-15 14:15:22.125496
9	28	ica	t	f	qr-verified	ica	2017-09-15 14:28:00.567927
10	28	ica	t	f	qr-verified	ica	2017-09-15 14:28:43.066532
11	28	ica	t	f	qr-verified	ica	2017-09-15 14:31:22.713
12	28	ica	t	f	qr-verified	ica	2017-09-15 14:33:25.102151
13	28	ica	t	f	qr-verified	ica	2017-09-15 14:46:33.65958
14	28	ica	t	f	qr-verified	ica	2017-09-15 14:58:59.749654
15	28	ica	t	f	qr-verified	ica	2017-09-15 15:02:27.867709
16	28	ica	t	f	qr-verified	ica	2017-09-15 15:04:36.487771
17	28	ica	t	f	qr-verified	ica	2017-09-15 15:08:01.112066
18	28	ica	t	f	qr-verified	ica	2017-09-15 15:09:50.538614
19	28	ica	t	f	qr-verified	ica	2017-09-15 15:11:46.540187
21	29	preste	t	f	qr-verified	preste	2017-09-15 16:59:20.481874
22	28	preste	t	f	qr-verified	preste	2017-09-15 17:05:04.594311
23	30	preste	t	f	qr-verified	preste	2017-09-15 17:06:52.196193
24	33	ica	t	f	qr-verified	ica	2017-09-18 10:32:59.27082
25	34	preste	t	f	qr-verified	preste	2017-09-18 10:36:30.609459
26	34	preste	t	f	qr-verified	preste	2017-09-18 10:42:05.151392
27	34	ica	t	f	qr-verified	ica	2017-09-18 11:08:26.762686
28	35	ica	t	f	qr-verified	ica	2017-09-18 13:00:46.694025
29	35	preste	t	f	qr-verified	preste	2017-09-18 13:25:13.324482
30	35	preste	t	f	qr-verified	preste	2017-09-18 13:35:09.483968
31	36	ica	t	f	qr-verified	ica	2017-09-18 13:37:42.355336
32	36	ica	t	f	qr-verified	ica	2017-09-18 13:39:56.15659
33	36	ica	t	f	qr-verified	ica	2017-09-18 13:41:11.938133
34	37	preste	t	f	qr-verified	preste	2017-09-18 13:44:56.717239
35	38	preste	t	f	qr-verified	preste	2017-09-18 13:49:25.931147
36	38	preste	t	f	qr-verified	preste	2017-09-18 13:55:08.104474
37	40	cashier01	t	f	qr-verified	cashier01	2017-09-18 14:34:30.345603
38	45	ica	t	f	qr-verified	ica	2017-09-18 14:35:53.047663
39	45	ica	t	f	qr-verified	ica	2017-09-18 15:06:53.101675
40	46	satria	t	f	qr-verified	satria	2017-09-18 15:10:59.409169
41	53	satria	t	f	qr-verified	satria	2017-09-18 15:12:05.585595
42	60	satria	t	f	qr-verified	satria	2017-09-18 15:14:57.108798
43	61	satria	t	f	qr-verified	satria	2017-09-18 15:34:18.853777
44	62	satria	t	f	qr-verified	satria	2017-09-18 15:43:08.740536
45	62	satria	t	f	qr-verified	satria	2017-09-18 15:44:02.714605
46	62	satria	t	f	qr-verified	satria	2017-09-18 15:50:41.552991
47	62	ica	t	f	qr-verified	ica	2017-09-18 16:09:12.821751
48	63	ica	t	f	qr-verified	ica	2017-09-18 16:14:59.516895
49	66	ica	t	f	qr-verified	ica	2017-09-18 16:23:00.291964
50	66	ica	t	f	qr-verified	ica	2017-09-18 16:23:11.752437
51	66	ica	t	f	qr-verified	ica	2017-09-18 16:25:47.480459
52	66	ica	t	f	qr-verified	ica	2017-09-18 16:27:02.370906
53	66	ica	t	f	qr-verified	ica	2017-09-18 16:27:11.730329
54	66	ica	t	f	qr-verified	ica	2017-09-18 16:27:48.590004
55	66	ica	t	f	qr-verified	ica	2017-09-18 16:27:57.770224
56	66	ica	t	f	qr-verified	ica	2017-09-18 16:28:49.829656
57	66	ica	t	f	qr-verified	ica	2017-09-18 16:30:44.948898
58	66	ica	t	f	qr-verified	ica	2017-09-18 16:31:04.273003
59	66	ica	t	f	qr-verified	ica	2017-09-18 16:33:30.772356
60	66	ica	t	f	qr-verified	ica	2017-09-18 16:35:00.799915
61	66	ica	t	f	qr-verified	ica	2017-09-18 17:15:26.298548
62	66	ica	t	f	qr-verified	ica	2017-09-18 17:15:57.240671
63	66	ica	t	f	qr-verified	ica	2017-09-18 17:24:03.020521
64	66	ica	t	f	qr-verified	ica	2017-09-18 17:27:07.603271
65	66	ica	t	f	qr-verified	ica	2017-09-18 17:33:20.395534
66	66	ica	t	f	qr-verified	ica	2017-09-18 17:37:45.365536
67	67	ica	t	f	qr-verified	ica	2017-09-18 17:43:17.977541
68	67	ica	t	f	qr-verified	ica	2017-09-18 17:53:53.731681
69	67	ica	t	f	qr-verified	ica	2017-09-18 17:58:58.650541
70	67	ica	t	f	qr-verified	ica	2017-09-18 18:59:01.487015
71	67	ica	t	f	qr-verified	ica	2017-09-18 19:01:29.749244
72	67	ica	t	f	qr-verified	ica	2017-09-18 19:06:06.048694
73	67	ica	t	f	qr-verified	ica	2017-09-18 19:08:53.627667
74	67	ica	t	f	qr-verified	ica	2017-09-18 19:20:52.577021
75	67	ica	t	f	qr-verified	ica	2017-09-18 19:22:27.704873
76	67	ica	t	f	qr-verified	ica	2017-09-18 19:24:32.87136
77	67	ica	t	f	qr-verified	ica	2017-09-18 19:27:23.667822
78	67	alhakam	t	f	qr-verified	alhakam	2017-09-18 19:33:48.674675
79	69	satria	t	f	qr-verified	satria	2017-09-18 19:40:21.647533
\.


--
-- Name: validated_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('validated_id_seq', 79, true);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

