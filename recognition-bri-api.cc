/*
 * =====================================================================================
 *
 *       Filename:  recognition-bri-api.cc
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  15/09/17 08:48:38
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Izzul Haq Alhakam (al), izzul@bahasakita.co.id
 *   Organization:  PT. Bahasa Kinerja Utama
 *
 * =====================================================================================
 */

#include "bri-api.h"

// Create General Objet from bri-api.h
pss obj;

int init_bri_api ( char *_api_key, char *_api_secret, char *_client_id,
                  char *_client_secret, char *_account_number) {

    std::string  api_key(_api_key);
    std::string  api_secret(_api_secret);
    std::string  client_id(_client_id);
    std::string  client_secret(_client_secret);
    std::string  account_number(_account_number);

    std::string url_api ("https://private-anon-8cd687e5dd-briapi.apiary-mock.com/v1/api/");

    // init ( &obj, API_KEY, API_SECRET, CLIENT_ID, CLIENT_SECRET
    bri_api::init( &obj, api_key, api_secret, client_id, client_secret );

    // Set base url
    bri_api::set_base_url( &obj, url_api);

    // set account number
    bri_api::set_account_number( &obj, account_number);

    // Get Token
    bri_api::job(&obj, GET_TOKEN);

  return 0;
}

std::string transfer_bri_api (char *_noreferal, char *_dest_rekening, char *_amount) {

    std::string noreferal(_noreferal);
    std::string dest_rekening(_dest_rekening);
    std::string amount(_amount);

    // Create Json
    std::stringstream ss;
    ss << "{\"noreferral\": \"";
    ss << noreferal ;
    ss << "\",""\"noRekTujuan\": \"";
    ss << dest_rekening;
    ss << "\",""\"amount\":\"";
    ss << amount;
    ss << "\"}";

    std::cout<< ss.str() <<std::endl;

    // Post transfer
    bri_api::job(&obj, POST_TRANSFER, ss.str());

    // get status transfer
    // obj->response;

    std::cout<<"obj.response : "<< obj.response <<std::endl;
    return obj.response;
}
