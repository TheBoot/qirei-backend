/*
 * =====================================================================================
 *
 *       Filename:  recognition-bri-api.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  18/09/17 16:27:49
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Izzul Haq Alhakam (al), izzul@bahasakita.co.id
 *   Organization:  PT. Bahasa Kinerja Utama
 *
 * =====================================================================================
 */

int init_bri_api ( char *_api_key, char *_api_secret, char *_client_id, char *_client_secret, char *_account_number);


std::string transfer_bri_api (char *_noreferal, char *_dest_rekening, char *_amount);
