/**
 * Filename: /home/raha/qere-socket/recognition-client-function.c
 * Path: /home/raha/qere-socket
 * Created Date: Monday, September 11th 2017, 3:53:19 pm
 * Author: raha
 * 
 * Copyright (c) 2017 PT Bahasa Kinerja Utama
 */
 #include <iostream>
 #include <cstring>
 #include "recognition.h"
 //#include "FaceRecognize.hpp"
 #include "recognition-face-enrollment.h"
 #include "recognition-face-verification.h"
 #include "recognition-bri-api.h" 
 //#include <string>
 
 #define BUFFER_LEN     1024 * 1000
 #define DB_CONN_STRING "user=root password=alhakam77 dbname=qrface"
 #define FOLDER_PHOTO   "/home/alhakam/Data/workspace/qirei-socket/frc_app/raw/"
 #define FOLDER_TRAINING "/home/alhakam/Data/workspace/qirei-socket/frc_app/train/"
 #define FOLDER_VERIFICATION "/home/alhakam/Data/workspace/qirei-socket/frc_app/verification/"
 #define FISHER_CSV "/home/alhakam/Data/workspace/qirei-socket/model/fisherface.csv"
 #define FISHER_MODEL "/home/alhakam/Data/workspace/qirei-socket/model/fisherface.yml"
 #define HAAR_CASCADE "/usr/local/share/OpenCV/haarcascades/haarcascade_frontalface_default.xml"
 #define CASCADE "/usr/local/share/OpenCV/haarcascades/haarcascade_frontalface_alt.xml"
 #define NESTED_CASCADE "/usr/local/share/OpenCV/haarcascades/haarcascade_eye_tree_eyeglasses.xml"

     // Get string by offset and len
     int select_str(char *dest, char *src, int offset, int sublen);

     int transaction_bri (struct lws *wsi, struct face_recognition_payment_data_monitoring_agent *frc, json_object *jo_req); 
     
     int check_dir(char * foldername) {
        /*char path[256];
        strcpy(path, FOLDER_PHOTO);
        strcat(path, foldername);*/

        lwsl_notice("path to check: %s", foldername);

        DIR *dir = opendir(foldername);
        if (dir) {
            closedir(dir);
            return 1;
        } else if (ENOENT == errno) {
            return 0;
        } else {
            lwsl_notice("something's wrong while checking for directory");
            return 1;
        }
        return 1;
     }

     int check_file_exists(char * filename) {
        FILE *file;
        
        file = fopen(filename, "r");
        if (file){
            return 0;
            fclose(file);
        } else {
            return 1;
        }

        return 1;
     } 

     int count_file_in_dir(char * foldername){
        int file_count = 0;
        DIR *dirp;
        struct dirent *entry;

        dirp = opendir(foldername);
        if (dirp){
            while((entry = readdir(dirp)) != NULL) {
                if (entry->d_type == DT_REG){
                    file_count++;
                }
            }
            closedir(dirp);
            return file_count;
        } else {
            lwsl_notice("something's wrong when opening directory");
            return 1;
        }
        return 1;
     }

     char * getDateYYMMDD() {
         time_t rawtime = time (NULL);
         struct tm *tm = localtime(&rawtime);
         char * date_string = (char *)malloc(8 * sizeof(char));
         //int    n;

         strftime(date_string, sizeof(date_string), "%y%m%d", tm);
         //date_string[n] ='\0';

         return date_string;
     }

     int open_db_connection(struct face_recognition_payment_data_monitoring_agent *frc){
        frc->conn = PQconnectdb(DB_CONN_STRING);
        if (PQstatus(frc->conn) == CONNECTION_BAD) {
            fprintf(stderr, "Connection to database failed: %s\n",
                PQerrorMessage(frc->conn));
        
                PQfinish(frc->conn);
                return -1;
        }
        lwsl_notice("Connection Opened.\n");
        return 0;
     }

     void write_to_browser(struct lws *wsi, struct face_recognition_payment_data_monitoring_agent *frc, char *label, json_object *value) {
         char out_brw[1024 * 100];
         json_object *jo_res;
          
         jo_res = json_object_new_object();
         json_object_object_add(jo_res, "label", json_object_new_string(label));
         json_object_object_add(jo_res, "value", value);

         lwsl_notice("write_to_browser_ lenth: %d", sizeof(json_object_to_json_string(jo_res)));
         int n = sprintf(out_brw, "%s", json_object_to_json_string(jo_res));

         out_brw[n] = '\0';
         enq(frc->q, out_brw);
         lwsl_notice(out_brw);

         json_object_put(jo_res);
         lwsl_notice("jo_res released!");
         lws_callback_on_writable(frc->wsi_ori);
         lwsl_notice("lws_callback_on_writeable executed!");
     }

    int buyer_login_response(struct lws *wsi,
        struct face_recognition_payment_data_monitoring_agent *frc, json_object *jo_req){

        PGresult    *res;
        char        *stm;
        char        *paramValues[3];
        char        p_value1[256];
        char        p_value2[256];
        int         n;

        json_object *obj_value;

        obj_value   = json_object_object_get(jo_req, "username");
        if(obj_value == NULL) return(-1);
        strcpy(p_value1, json_object_get_string(obj_value));

        obj_value   = json_object_object_get(jo_req, "password");
        if(obj_value == NULL) return(-1);
        strcpy(p_value2, json_object_get_string(obj_value));

        paramValues[0] = p_value1;
        paramValues[1] = p_value2;

        //create statement & execute it
        stm = (char *)"SELECT name, type, enrolled FROM users WHERE name = $1 AND password = $2";
        
        res = PQexecParams(frc->conn, stm, 2, NULL, 
                (const char * const*) paramValues, NULL, NULL, 0);
        
        if (PQresultStatus(res) == PGRES_TUPLES_OK) {
            n = PQntuples(res);

            if (n == 1) {
                obj_value = json_object_new_object();
                json_object_object_add(obj_value, "code",
                    json_object_new_string("00"));
                json_object_object_add(obj_value, "username",
                    json_object_new_string(PQgetvalue(res, 0, 0)));
                json_object_object_add(obj_value, "type",
                    json_object_new_string(PQgetvalue(res, 0, 1))); 
                json_object_object_add(obj_value, "enrolled",
                    json_object_new_string(PQgetvalue(res, 0, 2)));
                
                write_to_browser(wsi, frc, (char *)"buyer-login-response", obj_value);

                strcpy(frc->user_id, p_value1);
                //strcpy(frc->type, p_value3);
            } else {
                obj_value = json_object_new_object();
                json_object_object_add(obj_value, "code",
                    json_object_new_string("99"));
                write_to_browser(wsi, frc, (char *)"buyer-login-response", obj_value);
          
            }
        } else {
            lwsl_notice("login error");
        }

        PQclear(res);
        return 0;
    }


    int cashier_login_response(struct lws *wsi,
        struct face_recognition_payment_data_monitoring_agent *frc, json_object *jo_req){

        PGresult    *res;
        char        *stm;
        char        *paramValues[3];
        char        p_value1[256];
        char        p_value2[256];
        int         n;

        json_object *obj_value;

        obj_value   = json_object_object_get(jo_req, "username");
        if(obj_value == NULL) return(-1);
        strcpy(p_value1, json_object_get_string(obj_value));

        obj_value   = json_object_object_get(jo_req, "password");
        if(obj_value == NULL) return(-1);
        strcpy(p_value2, json_object_get_string(obj_value));

        paramValues[0] = p_value1;
        paramValues[1] = p_value2;

        //create statement & execute it
        stm = (char *)"SELECT name, type FROM users WHERE name = $1 AND password = $2";
        
        res = PQexecParams(frc->conn, stm, 2, NULL, 
                (const char * const*) paramValues, NULL, NULL, 0);
        
        if (PQresultStatus(res) == PGRES_TUPLES_OK) {
            n = PQntuples(res);

            if (n == 1) {
                obj_value = json_object_new_object();
                json_object_object_add(obj_value, "code",
                    json_object_new_string("00"));
                json_object_object_add(obj_value, "username",
                    json_object_new_string(PQgetvalue(res, 0, 0)));
                json_object_object_add(obj_value, "type",
                    json_object_new_string(PQgetvalue(res, 0, 1))); 
                write_to_browser(wsi, frc, (char *)"cashier-login-response", obj_value);

                strcpy(frc->user_id, p_value1);
                //strcpy(frc->type, p_value3);
            } else {
                obj_value = json_object_new_object();
                json_object_object_add(obj_value, "code",
                    json_object_new_string("99"));
                write_to_browser(wsi, frc, (char *)"cashier-login-response", obj_value);
          
            }
        } else {
            lwsl_notice("login error");
        }

        PQclear(res);
        return 0;
    }

    int buyer_registration_response(struct lws *wsi, struct face_recognition_payment_data_monitoring_agent *frc, json_object *jo_req){
        PGresult    *res;
        char        *stm;
        char        *paramValues[2];
        char        *p_value1;
        char        *p_value2;
        
        json_object *obj_value;

        obj_value   = json_object_object_get(jo_req, "username");
        if (obj_value == NULL) return(-1);
        p_value1 = (char *)json_object_get_string(obj_value);

        obj_value   = json_object_object_get(jo_req, "password");
        if(obj_value == NULL) return(-1);
        p_value2 = (char *)json_object_get_string(obj_value);

        paramValues[0] = p_value1;
        paramValues[1] = p_value2;

        stm = (char *)"INSERT INTO users(name, password, type, enrolled) VALUES($1, $2, 'buyer', 'false')";
         
        res = PQexecParams(frc->conn, stm, 2, NULL, (const char * const*) paramValues, NULL, NULL, 0);

        if (PQresultStatus(res) == PGRES_COMMAND_OK) {
            obj_value = json_object_new_object();
            json_object_object_add(obj_value, "code",
                json_object_new_string("00"));
            json_object_object_add(obj_value, "username",
                json_object_new_string(p_value1));

            write_to_browser(wsi, frc, (char *)"buyer-registration-response", obj_value);
        } else {
            obj_value = json_object_new_object();
            json_object_object_add(obj_value, "code",
                json_object_new_string("99"));

            write_to_browser(wsi, frc, (char*)"buyer-registration-response", obj_value);
        }

        PQclear(res);
        return 0;
    }

    int request_qr(struct lws *wsi, struct face_recognition_payment_data_monitoring_agent *frc, json_object *jo_req) {   
        PGresult    *res;
        char        *stm;
        char        *paramValues[4];
        char        p_value1[256];
        char        p_value2[256];
        char        qrCodeUniqueId[15];
        char        p_value1bckp[256];
        time_t      t;
        
        //uuid_t      uuid;
        //char        uuid_str[37];

        json_object *obj_value;
        
        srand((unsigned) time(&t));

        obj_value   = json_object_object_get(jo_req, "seller");
        if(obj_value == NULL) return(-1);
        strcpy(p_value1, json_object_get_string(obj_value));
    
        obj_value   = json_object_object_get(jo_req, "sell_value");
        if(obj_value == NULL) return(-1);
        strcpy(p_value2, json_object_get_string(obj_value));

        /*uuid_generate_time(uuid);
        sprintf(uuid_str, "%02x%02x%02x%02x-%02x%02x-%02x%02x-%02x%02x-%02x%02x%02x%02x%02x%02x", 
        uuid[0], uuid[1], uuid[2], uuid[3], uuid[4], uuid[5], uuid[6], uuid[7],
        uuid[8], uuid[9], uuid[10], uuid[11], uuid[12], uuid[13], uuid[14], uuid[15]
        );
        lwsl_notice("uuid_str: %s", uuid_str);*/

        //create qrcodes "uuid"
        strcpy(p_value1bckp, p_value1);
        paramValues[0] = p_value1;

        stm = (char *)"SELECT b.company_code, b.account_no FROM users a INNER JOIN seller_details b ON a.id = b.id_user WHERE a.name = $1 AND a.type = 'seller'";

        res = PQexecParams(frc->conn, stm, 1, NULL,
            (const char * const*) paramValues, NULL, NULL, 0);

        if (PQresultStatus(res) == PGRES_TUPLES_OK) {

           // qrCodeUniqueId[0] = '\0';
            memset(qrCodeUniqueId, 0, sizeof qrCodeUniqueId);
            qrCodeUniqueId[0] ='\0'; 
            
            int threeDigitRand = rand() % 900 + 100;
            lwsl_notice("threeDigitRand: %d", threeDigitRand);
            char strTDR[3];
            sprintf(strTDR, "%d", threeDigitRand); 
            lwsl_notice("strTDR: %s", strTDR);

            strcat(qrCodeUniqueId, strTDR);
            memset(strTDR, 0, sizeof strTDR);
            
            char companyCode[3];
            strcpy(companyCode, PQgetvalue(res, 0, 0));
            //char *companyCode = PQgetvalue(res, 0, 0);
            lwsl_notice("company code: %s", companyCode);
            
            strcat(qrCodeUniqueId, companyCode);
            memset(companyCode, 0, sizeof companyCode);

            char * date_string;
            date_string = getDateYYMMDD();
            lwsl_notice("date string: %s", date_string);

            strcat(qrCodeUniqueId, date_string);

            char account_str[13];
            strcpy(account_str, PQgetvalue(res, 0, 1));
            lwsl_notice("account_str: %s", account_str);
            int pos = 0;
            int length = 3;
            int ctr = 0;
            char account_str_sub[3];

            while (ctr < length) {
                account_str_sub[ctr] = account_str[pos+ctr];
                ctr++;
            }
            account_str_sub[ctr] = '\0';
            lwsl_notice("account_str_sub: %s", account_str_sub);

            strcat(qrCodeUniqueId, account_str_sub);
            memset(account_str_sub, 0, sizeof account_str_sub);

            lwsl_notice ("Qr codes id : %s", qrCodeUniqueId);
        } else {
            lwsl_notice("error");
            lwsl_notice(PQerrorMessage(frc->conn));
            return 0;
        }

        paramValues[0] = p_value1bckp;
        paramValues[1] = p_value2;
        paramValues[2] = qrCodeUniqueId;
        paramValues[3] = p_value1bckp;

        stm = (char *)"INSERT INTO qrcodes (seller, sell_value, uuid, created_by) VALUES ($1, $2, $3, $4)";
   
        res = PQexecParams(frc->conn, stm, 4, NULL, (const char* const*)paramValues, NULL, NULL, 0);

            
        if (PQresultStatus(res) == PGRES_COMMAND_OK) {
            obj_value = json_object_new_object();
            json_object_object_add(obj_value, "code",
                json_object_new_string("00"));
            json_object_object_add(obj_value, "uuid",
                json_object_new_string(qrCodeUniqueId));
            json_object_object_add(obj_value, "seller",
                json_object_new_string(p_value1bckp));
            json_object_object_add(obj_value, "sell_value",
                json_object_new_string(p_value2));
            
            write_to_browser(wsi, frc, (char *)"request-qr-response", obj_value);
        } else {
                lwsl_notice("Error: %s", PQerrorMessage(frc->conn));
                obj_value = json_object_new_object();
                json_object_object_add(obj_value, "code",
                    json_object_new_string("99"));
                write_to_browser(wsi, frc, (char *)"request-qr-response", obj_value);
        }

        PQclear(res);
        return 0;
    }

    int validate_qr(struct lws *wsi, struct face_recognition_payment_data_monitoring_agent *frc, json_object *jo_req) {
            
        PGresult    *res;
        char        *stm;
        char        *paramValues[4];
        char        p_value1[256];
        char        p_value2[256];
       // char        p_value3[256];    
        int         n;
        char        *qrid;
        char        *seller;
        char        *seller_company;
        char        *sell_value;

        //uuid_t      unique_id;
        //char        uuid_str[37];

        json_object *obj_value;

        obj_value   = json_object_object_get(jo_req, "uuid");
        if(obj_value == NULL) return(-1);
        strcpy(p_value1, json_object_get_string(obj_value));

        obj_value   = json_object_object_get(jo_req, "username");
        if(obj_value == NULL) return(-1);
        strcpy(p_value2, json_object_get_string(obj_value));

        paramValues[0] = p_value1;
        
        stm = (char *)"SELECT a.id, a.seller, a.uuid, d.company_code, a.sell_value FROM qrcodes a INNER JOIN (SELECT b.name, c.company_code FROM users b INNER JOIN seller_details c ON b.id = c.id_user) d on a.seller = d.name WHERE a.uuid = $1";

        res = PQexecParams(frc->conn, stm, 1, NULL, (const char * const*) paramValues, NULL, NULL, 0);

        if (PQresultStatus(res) == PGRES_TUPLES_OK) {
            n = PQntuples(res);

                if (n > 0) {
                qrid = PQgetvalue(res, 0, 0);
                seller = PQgetvalue(res, 0, 1);
                seller_company = PQgetvalue(res, 0, 3);
                sell_value = PQgetvalue(res, 0, 4);

                paramValues[0] = qrid;
                paramValues[1] = p_value2;
                paramValues[2] = p_value2;

                stm = (char *)"INSERT INTO validated(qrcodes_id, buyer, qr, face, status, created_by) VALUES($1, $2, true, false, 'qr-verified', $3)";
                
                res = PQexecParams(frc->conn, stm, 3, NULL, (const char * const*) paramValues, NULL, NULL, 0);

                if (PQresultStatus(res) == PGRES_COMMAND_OK) {
                    obj_value = json_object_new_object();
                    json_object_object_add(obj_value, "code",
                        json_object_new_string("00"));
                    json_object_object_add(obj_value, "company",
                        json_object_new_string(seller_company));
                    json_object_object_add(obj_value, "seller",
                        json_object_new_string(seller));
                    json_object_object_add(obj_value, "sell_value",
                        json_object_new_string(sell_value));
                    json_object_object_add(obj_value, "username",
                        json_object_new_string(p_value2));
                    
                    write_to_browser(wsi, frc, (char *)"validate-qr-response", obj_value);
                } else {
                    obj_value = json_object_new_object();
                    json_object_object_add(obj_value, "code",
                        json_object_new_string("99"));
                    write_to_browser(wsi, frc, (char *)"validate-qr-response", obj_value);
            
                }
            } else {
                obj_value = json_object_new_object();
                json_object_object_add(obj_value, "code",
                    json_object_new_string("99"));
                json_object_object_add(obj_value, "status",
                    json_object_new_string("Wrong QR codes."));
                write_to_browser(wsi, frc, (char *)"validate-qr-response", obj_value);
            }
        }

        PQclear(res);
        return 0;
    }

	int init_verification(struct lws *wsi, struct face_recognition_payment_data_monitoring_agent *frc, json_object *jo_req, char * uuid, char * username) {
        PGresult    *res;
        PGresult    *res2;
        PGresult    *res3;
        PGresult    *res4;
        char        *stm;
        char        *paramValues[3];
        char        *p_value1;
        char        *p_value2;
        char        *userid;
        char        *qrid;
        int         dir_status;
        int         label;
        int         enrolled_label;
        char        fisher_model_path[256];
        char        haar_cascade[256];
        char        image_path[256];
        double      confidence = 700;


        json_object *obj_value;

        /*obj_value = json_object_object_get(jo_req, "username");
        if(obj_value == NULL) return(-1);
        p_value1 = (char *)json_object_get_string(obj_value);

        obj_value = json_object_object_get(jo_req, "uuid");
        if(obj_value == NULL) return(-1);
        p_value2 = (char *)json_object_get_string(obj_value);*/

        p_value1 = (char *)username;
        p_value2 = (char *)uuid;

        paramValues[0] = p_value1;

        stm = (char *)"SELECT id FROM users WHERE name = $1";

        res = PQexecParams(frc->conn, stm, 1, NULL, (const char * const*) paramValues, NULL, NULL, 0);

        if (PQresultStatus(res) == PGRES_TUPLES_OK) { 
            char *prefix = (char *)"s";
            userid = PQgetvalue(res, 0, 0);

            char *combine;
            combine = (char *)malloc(strlen(userid)+1+1);
            strcpy(combine, prefix);
            strcat(combine, userid);
            lwsl_notice("combined path: %s", combine);

            char fullpath[256];
            strcpy(fullpath, FOLDER_VERIFICATION);
            strcat(fullpath, combine);
            strcat(fullpath, (char *)"/");

            dir_status = check_dir(fullpath);
            if (dir_status == 0) {
                lwsl_notice("please check your folder path");
                return 0;
            }

            strcpy(image_path, fullpath);
            //strcat(image_path, "/");
            strcat(image_path, p_value2);
            strcat(image_path, ".png");

            strcpy(haar_cascade, HAAR_CASCADE);
            strcpy(fisher_model_path, FISHER_MODEL);

            label = validate_face(wsi, frc, jo_req, image_path, fisher_model_path, haar_cascade, confidence);
            lwsl_notice("label %d", label);
            if (label != -1) {
                lwsl_notice("userid: %s", userid);
                paramValues[0] = userid;

                stm = (char *)"SELECT DISTINCT(LABEL) FROM enrolled_faces WHERE id_user = $1";

                res2 = PQexecParams(frc->conn, stm, 1, NULL, (const char * const*) paramValues, NULL, NULL, 0);

                if (PQresultStatus(res2) == PGRES_TUPLES_OK) {
                    enrolled_label = atoi(PQgetvalue(res2, 0, 0));
                    lwsl_notice("label: %d", label);
                    lwsl_notice("enrolled_label: %d", enrolled_label);

                    if (label == enrolled_label) {
                        paramValues[0] = p_value2;

                        stm = (char *)"SELECT DISTINCT(id) FROM qrcodes where uuid = $1";

                        res3 = PQexecParams(frc->conn, stm, 1, NULL, (const char * const*) paramValues, NULL, NULL, 0);

                        if (PQresultStatus(res3) == PGRES_TUPLES_OK){
                            qrid = PQgetvalue(res3, 0, 0);

                            paramValues[0] = qrid;
                            paramValues[1] = p_value1;

                            stm = (char *)"UPDATE validated SET face = 'true' AND status = 'all_verified' WHERE qrcodes_id = $1 AND buyer = $2 AND face = 'false'";
                            res4 = PQexecParams(frc->conn, stm, 2, NULL, (const char * const*) paramValues, NULL, NULL, 0);
                      
                            if (PQresultStatus(res4) == PGRES_COMMAND_OK) {
                                obj_value = json_object_new_object();
                                json_object_object_add(obj_value, "code",
                                    json_object_new_string("00"));
                                json_object_object_add(obj_value, "username",
                                    json_object_new_string(p_value1));
                                json_object_object_add(obj_value, "status",
                                    json_object_new_string("verification-success"));
                                
                                write_to_browser(wsi, frc, (char *)"init-verification-response", obj_value);

                                transaction_bri(wsi, frc, jo_req); 
                            }

                            PQclear(res4);
                        }

                        PQclear(res3);
                    } else {
                        obj_value = json_object_new_object();
                        json_object_object_add(obj_value, "code",
                            json_object_new_string("99"));
                        json_object_object_add(obj_value, "username",
                            json_object_new_string(p_value1));
                        json_object_object_add(obj_value, "status",
                            json_object_new_string("verification-failed-wrong-label"));
                        
                        write_to_browser(wsi, frc, (char *)"init-verification-response", obj_value);
                    }
                } else {
                    obj_value = json_object_new_object();
                    json_object_object_add(obj_value, "code",
                        json_object_new_string("99"));
                    json_object_object_add(obj_value, "status",
                        json_object_new_string("verification-failed-cannot-get-enrolled-label"));

                    write_to_browser(wsi, frc, (char *)"init-verification-response", obj_value);
                }

                PQclear(res2);
            } else {
                obj_value = json_object_new_object();
                json_object_object_add(obj_value, "code",
                    json_object_new_string("99"));
                json_object_object_add(obj_value, "username",
                    json_object_new_string(p_value1));
                json_object_object_add(obj_value, "status",
                    json_object_new_string("verification-failed-not-enrolled"));

                write_to_browser(wsi, frc, (char *)"init-verification-response", obj_value);
            }

            free(combine);
        }

        PQclear(res);
        return 0;
	}

    int send_image_start(struct lws *wsi, struct face_recognition_payment_data_monitoring_agent *frc, json_object *jo_req) {
        PGresult    *res;
        PGresult    *res2;
        char        *stm;
        char        *paramValues[3];
        char        *p_value1;
        char        *p_value2;
        char        *p_value3;
        char        *userid;
        //int         n;
        int         dir_status;

        json_object *obj_value;
        
        obj_value = json_object_object_get(jo_req, "filename");
        if(obj_value == NULL) return(-1);
        p_value1 = (char *)json_object_get_string(obj_value);

        obj_value = json_object_object_get(jo_req, "username");
        if(obj_value == NULL) return(-1);
        p_value2 = (char *)json_object_get_string(obj_value);

        obj_value = json_object_object_get(jo_req, "mode");
        if(obj_value == NULL) return(-1);
        p_value3 = (char *)json_object_get_string(obj_value);

        paramValues[0] = p_value2;

        stm = (char *)"SELECT id FROM users WHERE name = $1";

        res = PQexecParams(frc->conn, stm, 1, NULL, (const char * const*) paramValues, NULL, NULL, 0);

        if (PQresultStatus(res) == PGRES_TUPLES_OK) {
            char *prefix = (char *)"s";
            userid = PQgetvalue(res, 0, 0);

            char *combine;
            combine = (char *)malloc(strlen(userid)+1+1);
            strcpy(combine, prefix);
            strcat(combine, userid);
            lwsl_notice("combined path: %s", combine);

            char fullpath[256];
            
            if (strcmp(p_value3, "enroll") == 0) {
                strcpy(fullpath, FOLDER_PHOTO);
                strcat(fullpath, combine);
            } else if (strcmp(p_value3, "verification") == 0) {
                strcpy(fullpath, FOLDER_VERIFICATION);
                strcat(fullpath, combine);
            } else {
                lwsl_notice("please check mode.");
                return 0;
            }

            dir_status = check_dir(fullpath);
            if (dir_status == 0) {
                dir_status = mkdir(fullpath, 0777);
                if (dir_status == 0) {
                    char cmd[512]; 
                    strcpy(cmd,(char *)"chmod -R 777 ");
                    strcat(cmd, fullpath);
                    lwsl_notice("cmd1: %s", cmd);
                    lwsl_notice("folder created");
                    system(cmd);
                } else {
                    lwsl_notice("folder not created");
                }
            }

            strcat(fullpath,(char *)"/");
            strcat(fullpath, p_value1);

            strcpy(frc->file_name, fullpath);
            
            if(strcmp(p_value3, "verification") == 0) {
                int exists;
                exists = check_file_exists(fullpath);
                if (exists == 0) {
                    lwsl_notice("masuk nih");
                    char cmd[256];
                    strcpy(cmd, (char *)"rm ");
                    strcat(cmd, fullpath);
                    lwsl_notice("cmd: %s", cmd);
                    system(cmd);
                }
            }

            frc->fd = open(fullpath, O_CREAT|O_WRONLY|O_APPEND, S_IRUSR | S_IWUSR);

            if (strcmp(p_value3, "enroll") == 0) {
        /*        paramValues[0] = userid;
                paramValues[1] = p_value2;

                stm = (char *)"INSERT INTO enrolled_faces(id_user, created_by) VALUES($1, $2)";
                res2 = PQexecParams(frc->conn, stm, 2, NULL, (const char * const*) paramValues, NULL, NULL, 0);*/      
               // if (PQresultStatus(res2) == PGRES_COMMAND_OK) {
                    obj_value = json_object_new_object();
                    json_object_object_add(obj_value, "code",
                        json_object_new_string("00"));
                    json_object_object_add(obj_value, "file_name",
                        json_object_new_string(p_value1));
                    
                    write_to_browser(wsi, frc, (char *)"send-image-start-response", obj_value);
                /*} else {
                    obj_value = json_object_new_object();
                    json_object_object_add(obj_value, "code",
                        json_object_new_string("99"));

                    write_to_browser(wsi, frc, (char *)"send-image-start-response", obj_value);
                }*/
                //PQclear(res2);
            } else {
                obj_value = json_object_new_object();
                json_object_object_add(obj_value, "code",
                    json_object_new_string("00"));
                json_object_object_add(obj_value, "file_name",
                    json_object_new_string(p_value1));
                
                write_to_browser(wsi, frc, (char *)"send-image-start-response", obj_value);
            }

            PQclear(res);
            free(combine);
        }
        return 0;
    }

    int send_image_end(struct lws *wsi, struct face_recognition_payment_data_monitoring_agent *frc, json_object *jo_req) {
        json_object *obj_value;
        char        *p_value1;
        char        *p_value2;
        //int         n;

        //close fd
        close(frc->fd);
        lwsl_notice("frc->file_name: %s", frc->file_name);
        char cmd[512];
        strcpy(cmd, (char *)"chmod 777 ");
        strcat(cmd, frc->file_name);
        lwsl_notice("cmd2: %s", cmd);
        system(cmd);

        obj_value = json_object_object_get(jo_req, "username");
        if(obj_value == NULL) return(-1);
        p_value1 = (char *)json_object_get_string(obj_value);

        obj_value = json_object_object_get(jo_req, "mode");
        if(obj_value == NULL) return(-1);
        p_value2 = (char *)json_object_get_string(obj_value);

        obj_value = json_object_new_object();
        json_object_object_add(obj_value, "code",
            json_object_new_string("00"));
        if(strcmp(p_value2, "enroll") == 0) {
            json_object_object_add(obj_value, "file_name",
                json_object_new_string(frc->file_name));
        } else if (strcmp(p_value2, "verification") == 0) {
            json_object_object_add(obj_value, "username",
                json_object_new_string(p_value1));
        } else {
            json_object_object_add(obj_value, "warning",
                json_object_new_string("check-mode"));
        }
        json_object_object_add(obj_value, "mode",
            json_object_new_string(p_value2));

        write_to_browser(wsi, frc, (char *)"send-image-end-response", obj_value);
        
        if (strcmp(p_value2, "verification") == 0) {
            char uuid[16];
            int len;
            len = (strlen(frc->file_name)-19);
            select_str(uuid, frc->file_name, len, 15);
            lwsl_notice("uuid : %s\n", uuid);
            init_verification(wsi, frc, jo_req, uuid, p_value1);

        }
        return 0;
    }

    int init_enroll(struct lws *wsi, struct face_recognition_payment_data_monitoring_agent *frc, json_object *jo_req) {
        PGresult    *res;
        PGresult    *res2;
        char        *stm;
        char        *paramValues[5];
        char        *p_value1;
        int         label;
        char        raw_path[256];
        char        training_path[256];
        char        out_csv[256];
        char        out_model[256];
        char        haar_cascade[256];
        char        nested_cascade[256];
        char        enrolled_idx[256];
        char        enrolled_path[512];
        char        enrolled_label[128];
        char        useridfrompath[32];
        char        *userid;
        int         n;
        int         dir_status;

        json_object *obj_value;

        int arraylen, i;
        json_object *jo_req_enroll = json_object_new_object();
        json_object *obj_enrolled;
        json_object *obj_enrolled_idx;
        json_object *obj_enrolled_path;
        json_object *obj_enrolled_label;

        obj_value = json_object_object_get(jo_req, "username");
        if(obj_value == NULL) return(-1);
        p_value1 = (char *)json_object_get_string(obj_value);

        paramValues[0] = p_value1;

        stm = (char *)"SELECT id FROM users WHERE name = $1";

        res = PQexecParams(frc->conn, stm, 1, NULL, (const char * const*) paramValues, NULL, NULL, 0);

        if (PQresultStatus(res) == PGRES_TUPLES_OK) {
            char *prefix = (char *)"s";
            userid = PQgetvalue(res, 0, 0);

            char *combine;
            combine = (char *)malloc(strlen(userid)+1+1);
            strcpy(combine, prefix);
            strcat(combine, userid);
            lwsl_notice("combined path: %s", combine);

            char fullpath[256];
            strcpy(fullpath, FOLDER_PHOTO);
            strcat(fullpath, combine);
            strcat(fullpath, (char *)"/");
            strcpy(raw_path, fullpath);

            dir_status = check_dir(fullpath);
            if (dir_status == 0) {
                lwsl_notice("please check your folder path");
                return 1;
            }

            memset(fullpath, 0, sizeof fullpath);
            strcpy(fullpath, FOLDER_TRAINING);
            strcat(fullpath, combine);
            strcat(fullpath, (char *)"/");
            strcpy(training_path, fullpath);

            dir_status = check_dir(fullpath);
            if (dir_status == 0) {
                dir_status = mkdir(fullpath, 0777);
                if (dir_status == 0) {
                    char cmd[512];
                    strcpy(cmd,(char *)"chmod -R 777 ");
                    strcat(cmd, fullpath);
                    lwsl_notice("cmd1: %s", cmd);
                    system(cmd);
                    lwsl_notice("folder training created");
                } else {
                    lwsl_notice("folder training not created");
                    return 1;
                }
            }

            strcpy(out_csv, FISHER_CSV);
            strcpy(out_model, FISHER_MODEL);
            strcpy(haar_cascade, CASCADE);
            strcpy(nested_cascade, NESTED_CASCADE);

            //memset(training_path, 0, sizeof(training_path));
            //strcpy(training_path, FOLDER_TRAINING);

            lwsl_notice("raw_path: %s", raw_path);
            lwsl_notice("training_path: %s", training_path);
           
            ///  IZZUL CHANGED ///
            // Parsing Array JSON object from enrollment face
            // insert here (variable to get json array enrolle face)
            enrollment_face(wsi, frc, &jo_req_enroll, raw_path, training_path, out_csv,out_model, haar_cascade, nested_cascade);
            

            obj_enrolled =  json_object_object_get(jo_req_enroll, "enrolled");

            //lwsl_notice("%s", json_object_to_json_string(obj_enrolled));

            // get lengt array
            arraylen = json_object_array_length(obj_enrolled);

            // get array data
            for(i=0; i<arraylen; i++) {
                // get index i-th object in obj_enrolled
                lwsl_notice("data ke -%d\n",i);
                obj_enrolled_idx = json_object_array_get_idx(obj_enrolled, i);
                strcpy(enrolled_idx, (char *)json_object_get_string(obj_enrolled_idx));
                lwsl_notice("done obj_enrolled_idx");
                // get the path attribute in i-th object
                obj_enrolled_path = json_object_object_get(obj_enrolled_idx, "path");
                strcpy(enrolled_path, (char *)json_object_get_string(obj_enrolled_path));
                lwsl_notice("done obj_enrolled_path");
                // get the label attribute in i-th object
                obj_enrolled_label = json_object_object_get(obj_enrolled_idx, "label");
                strcpy(enrolled_label, (char *)json_object_get_string(obj_enrolled_label));
                lwsl_notice("done obj_enrolled_label");

                lwsl_notice("enrolled_label ke-%d: %s", i,enrolled_label);
                lwsl_notice("enrolled_path ke-%d: %s", i,enrolled_path);
                

                paramValues[0] = enrolled_path;
                
                stm = (char *)"SELECT id_user FROM enrolled_faces where path = $1";

                res = PQexecParams(frc->conn, stm, 1, NULL, (const char * const*) paramValues, NULL, NULL, 0);

                if (PQresultStatus(res) == PGRES_TUPLES_OK){
                    n = PQntuples(res);

                    if (n == 0) {
                        PQclear(res);
                        select_str(useridfrompath, enrolled_path, 57, 1);
                        lwsl_notice("useridfrompath: %s", useridfrompath);

                        paramValues[0] = useridfrompath;
                        paramValues[1] = enrolled_path;
                        paramValues[2] = enrolled_label;
                        paramValues[3] = p_value1;

                        stm = (char *)"INSERT INTO enrolled_faces(id_user, path, label, created_by) VALUES($1, $2, $3, $4)";
                        res = PQexecParams(frc->conn, stm, 4, NULL, (const char * const*) paramValues, NULL, NULL, 0);
                    } else {
                        PQclear(res);
                        paramValues[0] = enrolled_label;
                        paramValues[1] = enrolled_path; 

                        stm = (char *)"UPDATE enrolled_faces SET label = $1 WHERE path = $2";
                        
                        res = PQexecParams(frc->conn, stm, 2, NULL, (const char * const*) paramValues, NULL, NULL, 0);
                    }
                    if (PQresultStatus(res) == PGRES_COMMAND_OK) {
                        lwsl_notice("success update label");
                        //free(useridfrompath);
                        json_object_put(obj_enrolled_idx);
                        json_object_put(obj_enrolled_path);
                        json_object_put(obj_enrolled_label);
                        memset(enrolled_idx, 0, sizeof enrolled_idx);
                        memset(enrolled_path, 0, sizeof enrolled_path);
                        memset(enrolled_label, 0, sizeof enrolled_label);
                    } else {
                        lwsl_notice("error: %s", PQerrorMessage(frc->conn));
                        return 0;
                    }
                }
            }
            /// END OF IZZUL CHANGED ////
            int count_trained_files;

            count_trained_files = count_file_in_dir(training_path);

            if (count_trained_files >= 10) {
                paramValues[0] = userid;

                stm = (char *)"UPDATE users set enrolled = 'true' WHERE id = $1";

                res2 = PQexecParams(frc->conn, stm, 1, NULL, (const char * const*) paramValues, NULL, NULL, 0); 
                if (PQresultStatus(res2) == PGRES_COMMAND_OK){
                    obj_value = json_object_new_object();
                    json_object_object_add(obj_value, "code",
                        json_object_new_string("00"));
                    json_object_object_add(obj_value, "username",
                        json_object_new_string(p_value1));
                    json_object_object_add(obj_value, "status",
                        json_object_new_string("done-enroll"));
                } else {
                    lwsl_notice("problem update table users");
                }
            } else {
                obj_value = json_object_new_object();
                json_object_object_add(obj_value, "code",
                    json_object_new_string("99"));
                json_object_object_add(obj_value, "username",
                    json_object_new_string(p_value1)); 
                json_object_object_add(obj_value, "status",
                    json_object_new_string("enroll-again"));
            }
            
            write_to_browser(wsi, frc, (char *)"init-enroll-response", obj_value);
            PQclear(res2);
            free(combine);
        }
       
        PQclear(res);
        return 0;
    }

int transaction_bri (struct lws *wsi, struct face_recognition_payment_data_monitoring_agent *frc, json_object *jo_req) {

    PGresult    *res;
    char        *stm;
    char        *paramValues[2];
    char        *p_value1;
    char        *p_value2;
    char        *p_value3;
    char        *p_value4;
    char        *p_value5;
    char        *p_value6;
    char        *p_value7;
    char        *p_value8;
    char        *p_value9;

    json_object *return_object; 
    json_object *obj_value;
    json_object *obj_data;

    // variable BRI
    char        api_key[256];
    char        api_secret[256];
    char        client_id[256];
    char        client_secret[256];
    char        account_number[256];
    
    char        buyer_id[256];
    char        seller_id[256];

    char        referal_num[256];
    char        dest_rekening[256];
    char        amount[256];
    int         rc;
    std::string return_transaction;

    // Set User BRI API
    // API KEY / BRI X-KEY
    strcpy(api_key, (char *)"84a1c96ba96e88bba5061b8f6d46179374011d23");
    // API SECRET /  Authorization
    strcpy(api_secret, (char *)"a0ad99beb5ce87dad402558ae767bd4420776333");
    // CLIENT ID / APP ID
    strcpy(client_id, (char *)"a3091df812146696641f2e73e9d3652eb4b4");
    // CLIENT SECRET / APP SECRET
    strcpy(client_secret, (char *)"b4b2b223da942db3d81f5251ac5e1eaf6bcc");
    // ACCOUNT NUMBER / No Rekening
    strcpy(account_number, (char *)"888801000105501");
    //  END OF SET USER BRI API //
    
    // transfer bri api const 
    // referal_num
    strcpy(referal_num, (char *)"11112341512");
    strcpy(dest_rekening, (char *)"032901012345501");
    strcpy(amount, (char *)"100");

    // init bri api
    rc = init_bri_api (api_key, api_secret, client_id, client_secret, account_number);

    // transfer bri api
    if (rc == 0) {
        // Te, ini ambil jadi json kamu yak yang dari barcode
        // untuk yang destinasi rekening, referal number sama amountnya
        // nilai balikan transfer adalah json string
        return_transaction = transfer_bri_api(referal_num, dest_rekening, amount);
        // parsing jsonya dari retrun transaction
        // here, trus masukin ke db
        //   {
        //       "status": true,
        //       "responseDescription": "Payment Success",
        //       "responseCode": "0200",
        //       "data": {
        //                "noreferral": "11112341512",
        //                "noRekAsal": "032901012345501",
        //                "noRekTujuan": "020601121234503",
        //                "amount": "100",
        //                "remark": "tes",
        //                "namaTrx": "API Tes"
        //                }
        //   }
        //
        return_transaction.erase(return_transaction.begin());
        return_transaction.erase(return_transaction.size() -1);

        //std::cout << "return transaction: " << return_transaction << std::endl; 

        return_object = json_tokener_parse(return_transaction.c_str());

        // Parsing No Referral
        obj_value = json_object_object_get(return_object, "status"); 
        if(obj_value == NULL)
            return(-1);
        p_value1 = (char *)json_object_get_string(obj_value);

        obj_value = json_object_object_get(return_object, "responseDescription");
        if(obj_value == NULL)
            return(-1);
        p_value2 = (char *)json_object_get_string(obj_value);

        obj_value = json_object_object_get(return_object, "responseCode");
        if(obj_value == NULL)
            return(-1);
        p_value3 = (char *)json_object_get_string(obj_value);
        
        //lwsl_notice("p_value 1: %s", p_value1);
        //lwsl_notice("p_value 2: %s", p_value2);
        //lwsl_notice("p_value 3: %s", p_value3);

        obj_value = json_object_object_get(return_object, "data");
        if(obj_value == NULL)
            return(-1);
            obj_data = json_object_object_get(obj_value, "noreferral");
            if(obj_data == NULL)
                return(-1);
            p_value4 = (char *)json_object_get_string(obj_data);

            obj_data = json_object_object_get(obj_value, "noRekAsal");
            if(obj_data == NULL)
                return(-1);
            p_value5 = (char *)json_object_get_string(obj_data);

            obj_data = json_object_object_get(obj_value, "noRekTujuan");
            if(obj_data == NULL)
                return(-1);
            p_value6 = (char *)json_object_get_string(obj_data);

            obj_data = json_object_object_get(obj_value, "amount");
            if(obj_data == NULL)
                return(-1);
            p_value7 = (char *)json_object_get_string(obj_data);

            obj_data = json_object_object_get(obj_value, "remark");
            if(obj_data == NULL)
                return(-1);
            p_value8 = (char *)json_object_get_string(obj_data);
    
            obj_data = json_object_object_get(obj_value, "namaTrx");
            if(obj_data == NULL)
                return(-1);
            p_value9 = (char *)json_object_get_string(obj_data);
    }
    else {
        printf("transaction failed\n");
        return -1;
    }
    
    /*  
    paramValues[0] = p_value5;

    stm = (char *)"SELECT id_user FROM buyer_details WHERE account_no = $1";
    
    res = PQexecParams(frc->conn, stm, 1, NULL, (const char * const*) paramValues, NULL, NULL, 0); 
    
    if (PQresultStatus(res) == PGRES_TUPLES_OK) {
       strcpy(buyer_id, PQgetvalue(res, 0, 0));
    } else {
        lwsl_notice("%s",PQerrorMessage(frc->conn));
        return -1;
    }

    PQclear(res);

    paramValues[0] = p_value6;

    stm = (char *) "SELECT id_user FROM seller_details WHERE account_no = $1";

    res = PQexecParams(frc->conn, stm, 1, NULL, (const char * const*) paramValues, NULL, NULL, 0);
    if (PQresultStatus(res) == PGRES_TUPLES_OK) {
        strcpy(seller_id, PQgetvalue(res, 0, 0));
    } else {
        lwsl_notice("%s", PQerrorMessage(frc->conn));
        return -1;
    }

    PQclear(res);

    paramValues[0] = seller_id;
    paramValues[1] = buyer_id;
    paramValues[2] = p_value4;
    paramValues[3] = p_value7;

    stm = (char *)"INSERT INTO transactions(seller_id, buyer_id, referral_no, value, created_by) VALUES($1, $2, $3, $4, 'bri')";

    res = PQexecParams(frc->conn, stm, 4, NULL, (const char * const*) paramValues, NULL, NULL, 0);

    */
    // With DB
    //if(PQresultStatus(res) == PGRES_COMMAND_OK) {

    // Without DB
    if(strcmp(p_value2,"Payment Success")==0) {
        obj_value = json_object_new_object();
        json_object_object_add(obj_value, "code",
            json_object_new_string("00"));
        json_object_object_add(obj_value, "buyer_account_no",
            json_object_new_string(p_value4));
        json_object_object_add(obj_value, "seller_account_no",
            json_object_new_string(p_value5));
        json_object_object_add(obj_value, "status",
            json_object_new_string("transaction-success"));

        write_to_browser(wsi, frc, (char *)"transaction-bri-response", obj_value);
    } else {
        obj_value = json_object_new_object(); 
        json_object_object_add(obj_value, "code",
            json_object_new_string("99"));
        json_object_object_add(obj_value, "status",
            json_object_new_string("transaction-failed"));
    
        write_to_browser(wsi, frc, (char *)"transaction-bri-response", obj_value);
    }
    // Free Memory usage of api
   /*free(api_key);
    free(api_secret);
    free(client_id);
    free(client_secret);
    free(account_number);
   */

    //PQclear(res);
    return 0;

}


int select_str(char *dest, char *src, int offset, int sublen)
{
  int length;
  int length2;

  /* Get length of string */
  length = strlen(src);

  if ( offset < 0 || offset >= length ) {
      dest[0] = '\0';
      return 0;
  }

  // array bound checking
  length2 = sublen + offset;
  if ( length2 > length ) {
      // truncate to maximum sublen if necessary
      sublen = length - offset;
  }

  // copy from source starting at offset with length sublen to dest
  memcpy(dest, &src[offset], sublen);
  // NULL terminated/
  dest[sublen] = '\0';

  return sublen;
} // int select_str(char *dest, char *src, int offset, int sublen)






