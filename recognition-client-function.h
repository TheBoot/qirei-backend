int open_db_connection(struct face_recognition_payment_data_monitoring_agent *frc);
int buyer_login_response(struct lws *wsi,
    struct face_recognition_payment_data_monitoring_agent *frc, json_object *jo_req);
int cashier_login_response(struct lws *wsi,
    struct face_recognition_payment_data_monitoring_agent *frc, json_object *jo_req);
int request_qr(struct lws *wsi, struct face_recognition_payment_data_monitoring_agent *frc, json_object *jo_req);
int validate_qr(struct lws *wsi, struct face_recognition_payment_data_monitoring_agent *frc, json_object *jo_req);
int send_image_start(struct lws *wsi, struct face_recognition_payment_data_monitoring_agent *frc, json_object *jo_req);
int send_image_end(struct lws *wsi, struct face_recognition_payment_data_monitoring_agent *frc, json_object *jo_req);
int init_enroll(struct lws *wsi, struct face_recognition_payment_data_monitoring_agent *frc, json_object *jo_req);   
int buyer_registration_response(struct lws *wsi, struct face_recognition_payment_data_monitoring_agent *frc, json_object *jo_req);
int init_verification(struct lws *wsi, struct face_recognition_payment_data_monitoring_agent *frc, json_object *jo_req);