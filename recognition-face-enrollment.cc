/*
 * =====================================================================================
 *
 *       Filename:  recognition-face-enrollment.c
 *
 *    Description:  Enroll Function Face Recognition
 *
 *        Version:  1.0
 *        Created:  13/09/17 10:06:54
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Izzul Haq Alhakam (al), izzul@bahasakita.co.id
 *   Organization:  PT. Bahasa Kinerja Utama
 *
 * =====================================================================================
 */

#include "recognition.h"
#include "recognition-client-function.h"
#include "FaceRecognize.hpp"


int enrollment_face(struct lws *wsi, struct face_recognition_payment_data_monitoring_agent *frc, json_object **jo_req, char * src_face, char * dest_face, char *outcsv, char *outmodel, char * haar_cascade, char * nested_cascade) {

        std::string      source(src_face);
        std::string      dest(dest_face);
        std::string      HAAR_CASCADE_ADDR(haar_cascade);
        std::string      NESTED_CASCADE_ADDR(nested_cascade);

        std::string      outcsvfile(outcsv);
        std::string      filemodel(outmodel);
        std::string      lastpath;
        int label;

        // Get Address Image Face Source Directory
        // Original Face From app


        // Get Address Destination Directory
        // Grayscale Face From Preparation
         std::string source_train("/home/alhakam/Data/workspace/qirei-socket/frc_app/train/");


        // Get Address Cascade File (haarcascade_frontalface_alt.xml)


        // Get Address Nested Cascade (haarcascade_eye_tree_eyeglasses.xml)


        // Set Model Name
        // Train Preparation
        // Save Grayscale face image to Destination Directory
        // if successed return 1
        //trainPreparation(HAAR_CASCADE_ADDR, NESTED_CASCADE_ADDR, source, dest, 2, true);
        //trainPreparation(HAAR_CASCADE_ADDR, NESTED_CASCADE_ADDR, source, dest, 2, false);
        trainPreparation(HAAR_CASCADE_ADDR, NESTED_CASCADE_ADDR, source, dest, 2);

        // Create CSV
        // create file csv from dest_face
        // if successed return 1
        // createCSV(dest_face, outcsvfile);
        createCSV(source_train, outcsvfile);

        // Train Data by fisherface
        // if successed return 1
        fisherfaceTrain(outcsvfile, filemodel);

        // get label from image train
        // and save to db by dest_face direcotry
        // return label if successed
        *jo_req = getlabelall(outcsvfile);
        
        std::cout<<std::endl;
        std::cout<<std::endl;
        //std::cout<< "data enroll : " <<json_object_to_json_string(*jo_req) << std::endl;

        lastpath = SplitFilename(dest);
        label = atoi(getlabel(outcsvfile, lastpath).c_str());

       return label;
}
