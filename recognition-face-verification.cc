/*
 * =====================================================================================
 *
 *       Filename:  recognition-face-verification.cc
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  13/09/17 14:32:47
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Izzul Haq Alhakam (al), izzul@bahasakita.co.id
 *   Organization:  PT. Bahasa Kinerja Utama
 *
 * =====================================================================================
 */

#include "recognition.h"
#include "recognition-client-function.h"
#include "recognition-face.h"

int validate_face(struct lws *wsi, struct face_recognition_payment_data_monitoring_agent *frc, json_object *jo_req, char * image_path, char * fisher_model_path, char * haar_cascade, double confidence) {


    int label;
    std::string classifier(haar_cascade);
    std::string modelface(fisher_model_path);
    std::string imageaddr(image_path);



    // Get Haarcascade default model

    // Get address model face

    // Get image verified

    // Set Confidence Level

    // call function faceRecognition
    // retrun label by image receive
    label = faceRecognition(classifier, modelface, imageaddr, confidence);
 
    return label;
}
