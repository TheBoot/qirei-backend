/*
 * =====================================================================================
 *
 *       Filename:  recognition-face.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  14/09/17 17:44:34
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Izzul Haq Alhakam (al), izzul@bahasakita.co.id
 *   Organization:  PT. Bahasa Kinerja Utama
 *
 * =====================================================================================
 */

#include <opencv2/core.hpp>
#include <opencv2/face.hpp>
#include <opencv2/face/facerec.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/objdetect.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/imgproc.hpp>
#include <sys/stat.h>
#include <dirent.h>
#include <iostream>
#include <stdio.h>
#include <string>
#include <sstream>

using namespace cv;

using std::cout;
using std::cerr;
using std::endl;
using std::string;
using std::vector;
using std::ifstream;
using std::stringstream;

int faceRecognition(string &classifier, string &modelface, string &imagefile,
                    double confidence_level) {

    unsigned int i;
    int label = -1;
    double confidence = 0;
    vector<Rect> faces;

    CascadeClassifier face_cascade;
    Mat OrigFace;
    Mat GrayscaleFace;


    // Using Fisherface recognition
    // cv::Ptr<cv::face::FaceRecognizer> model = cv::face::FisherFaceRecognizer::create(0, confidence_level);
    cv::Ptr<cv::face::FaceRecognizer> model = cv::face::FisherFaceRecognizer::create();

    // Read fisherface algorithm
    model->read(modelface);

    // Read File Classifier
    cout <<"classifier" << classifier << endl;
    if (!face_cascade.load(classifier)) {
        cout << "Error loading file" << endl;
        return -1;
    }

    // Get Image
    OrigFace = imread(imagefile);

    cout << "addr from facerecognition :" << imagefile << endl;

    /* Normalize Image */
    // Convert Image to grayscale
    cvtColor(OrigFace, GrayscaleFace, CV_BGR2GRAY);

    // Detect Image in gray image
    face_cascade.detectMultiScale(GrayscaleFace, faces, 1.1, 5, 0|CASCADE_SCALE_IMAGE, cv::Size(100,100));

    // Number Face Detected
    cout << faces.size() << " Faces Detected" << endl;
    //if (faces.size() > 0) {
        for (i=0; i<faces.size(); i++) {

            // Region Of Interest
            Rect face_i = faces[i];

            // Crop the ROI from Gray Image
            Mat face = GrayscaleFace(face_i);

            // Resizing the cropped image to suit
            // database image sizes
            Mat face_resized;
            resize(face, face_resized, Size(100, 100), 1.0, 1.0, INTER_CUBIC);

            // Recognizing what faces detected
            model->predict(face_resized, label, confidence);

            cout << " Threshold : " << model->getThreshold() << endl;
            cout << " confidence : " << confidence << endl;
            cout << " label : " << label << endl;
        }
    //} else {
    //    cout << " Cannot Faces Detected" << endl;
    //    label = -1;
    //    return label;
    //}

    // return value of label
    return label;
}
