/**
 * Filename: /home/raha/qere-socket/recognition-queue.c
 * Path: /home/raha/qere-socket
 * Created Date: Tuesday, September 12th 2017, 9:55:22 am
 * Author: raha
 * 
 * Copyright (c) 2017 PT Bahasa Kinerja Utama
 */

#include <assert.h>
#include "recognition-queue.h"

struct nqueue *queueCreate(void)
{
    struct nqueue *q;
    
    q = (struct nqueue *) malloc(sizeof(struct nqueue));
    q->head = q->tail = 0;     

    return q;
}

/* push a new value onto top of Queue */
void enq(struct nqueue *q, char *out_brw)
{
    //printf("masuk enq");     
    //printf("%s", out_brw);   
    int n;
    struct elt *e;

    e = (struct elt *) malloc(sizeof(struct elt));
    assert(e);

    n = sprintf(e->out_brw, "%s",out_brw);
    e->out_brw[n] = '\0';    

    e->len = n;

    /* Because I will be the tail, nobody is behind me */
    e->next = 0;
    if(q->head == 0) {
        /* If the queue was empty, I become the head */
        q->head = e;
    } else {
        /* Otherwise I get in line after the old tail */
        q->tail->next = e;
    }

    /* I become the new tail */
    q->tail = e;
}

int queueEmpty(const struct nqueue *q)
{
    return (q->head == 0);
}


int deq(struct nqueue *q, char *out_brw, int *len)
{
    int n;
    int ret;

    struct elt *e;
    assert(!queueEmpty(q));

    n=sprintf(out_brw, "%s", q->head->out_brw);
    out_brw[n] = '\0';
    *len = q->head->len;

    /* patch out first element */
    e = q->head;
    q->head = e->next;

    free(e);
    ret = 0;
    return ret;
}

/* print contents of queue on a single line, head first */
void queuePrint(struct nqueue *q)
{
    struct elt *e;

    for(e = q->head; e != 0; e = e->next) {
        printf("len: %d, out_brw: %s\n", e->len, e->out_brw);
        putchar('\n');
    }
}

/* free a queue and all of its elements */
void queueDestroy(struct nqueue *q)
{
    char    out_brw[1024 * 1000];
    int     len;

    while(!queueEmpty(q)) {
        deq(q, &out_brw[0], &len);
    }

    free(q);
}

