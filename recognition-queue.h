/**
 * Filename: /home/raha/qere-socket/recognition-queue.h
 * Path: /home/raha/qere-socket
 * Created Date: Tuesday, September 12th 2017, 9:58:08 am
 * Author: raha
 * 
 * Copyright (c) 2017 PT Bahasa Kinerja Utama
 */

#include    <stdio.h>
#include    <stdlib.h>
#include    <string.h>

struct elt {
    struct elt *next;
    char    out_brw[1024 * 1000];
    int     len;
    int     is_regular_response;
};

struct nqueue {
    struct elt *head;
    struct elt *tail;
};

struct nqueue *queueCreate(void);
void enq(struct nqueue *q, char *out_brw);
int queueEmpty(const struct nqueue *q);
int deq(struct nqueue *q, char *out_brw, int *len);
void queuePrint(struct nqueue *q);
void queueDestroy(struct nqueue *q);                                                                                                              