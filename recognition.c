/**
 * Filename: /home/raha/qere-socket/recognition.c
 * Path: /home/raha/qere-socket
 * Created Date: Monday, September 11th 2017, 1:38:36 pm
 * Author: raha
 * 
 * Copyright (c) 2017 PT Bahasa Kinerja Utama
 */


#include "recognition.h"

int close_testing;
int debug_level = 6;
volatile int force_exit = 0;
struct lws_context * context;

/* list of supported protocols and callbacks */
static struct lws_protocols protocols[] = {
    {
        "frp-monitoring-alpha1",
        callback_monitoring_agent,
        sizeof(struct face_recognition_payment_data_monitoring_agent),
        1024 * 1000,
    },
    { NULL, NULL, 0, 0 } /* terminator */
};

void sighandler(int sig) {
    force_exit = 1; 
    lws_cancel_service(context);
}

static struct option options[] = {
    {"help",        no_argument,        NULL, 'h'},
    {"debug",       required_argument,  NULL, 'd'},
    {"port",        required_argument,  NULL, 'p'},
#ifndef LWS_NO_DAEMONIZE
    {"daemonize",   no_argument,        NULL, 'D'},
#endif
    {NULL, 0, 0, 0}
};

int main(int argc, char **argv) {
    struct lws_context_creation_info info;
    int uid = -1, gid = -1; 
    int opts = 0;
    int n = 0;
    unsigned int ms, oldms = 0;
#ifndef _WIN32
    int syslog_options = LOG_PID | LOG_PERROR;
#endif
#ifndef LWS_NO_DAEMONIZE
    int daemonize = 0;
#endif

    /* 
        take care to zero down the info struct, he contains random garbage 
        from the stack otherwise
    */
    memset(&info, 0, sizeof info);
    info.port = 9070;

    while (n >= 0) {
        n = getopt_long(argc, argv, "ei:hp:d:D:u:g:", options, NULL);
        if (n < 0)
            continue;          
        switch (n) {
#ifndef LWS_NO_DAEMONIZE
            case 'D':
                demonize = 1;
                #ifndef _WIN32
                syslog_options &= ~LOG_PERROR;
                #endif
                break;
#endif
            case 'd': 
                debug_level = atoi(optarg);
                break;
            case 'p':
                info.port = atoi(optarg);
                break;
            case 'h':
                fprintf(stderr, "Usage: qrcode and face recognition");
                exit(1);
        }
    }
    

#if !defined(LWS_NO_DAEMONIZE) && !defined(WIN32)
    /** normally lock path would be /var/lock/lwsts or similar, to
     *  simplify getting started without having to take care about 
     *  permissions or running as root, set to /tmp/.lwsts-lock
     */
    if (daemonize && lws_daemonize("/tmp/.lwsts-lock")) {
        fprintf(stderr, "Failed to daemonize\n");
        return 10;
    }
#endif

    signal(SIGINT, sighandler);

#ifndef _WIN32
    /** we will only try to log things according to our debug_level */
    setlogmask(LOG_UPTO(LOG_DEBUG));
    openlog("lwsts", syslog_options, LOG_DAEMON);
#endif

    /** tell the library what debug level to emit and to send it to syslog */
    lws_set_log_level(debug_level, lwsl_emit_syslog);
    lwsl_notice("Face Qr Recognition Alpha 1\n");

    info.protocols = protocols;
    info.gid = gid;
    info.uid = uid;
    info.max_http_header_pool = 16;
    info.options = opts | LWS_SERVER_OPTION_VALIDATE_UTF8;
    info.timeout_secs = 5;

    context = lws_create_context(&info);
    if (context == NULL) {
        lwsl_err("ERROR -libwebsocket init failed\n");
        return -1;
    }

    n = 0;
    while (n >= 0 && !force_exit) {
        struct timeval tv;
        gettimeofday(&tv, NULL);
        
        ms = (tv.tv_sec * 1000) + (tv.tv_usec / 1000);
        if ((ms - oldms ) > 1000) {
            lws_callback_on_writable_all_protocol(context, &protocols[0]);
            oldms = ms;
        }
        n = lws_service(context, 10);
    }
    lws_context_destroy(context);
    lwsl_notice("Face Qr Recognition was exited cleanly\n");

#ifndef _WIN32
    closelog();
#endif 
    return 0;
}