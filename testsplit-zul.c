/*
 * =====================================================================================
 *
 *       Filename:  testsplit.c
 *
 *    Description:  test split from c
 *
 *        Version:  1.0
 *        Created:  29/09/17 09:29:59
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Izzul Haq Alhakam (al), izzul@bahasakita.co.id
 *   Organization:  PT. Bahasa Kinerja Utama
 *
 * =====================================================================================
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

int main()
{
    char str[512];
    char splitStrings[15][128]; //can store 10 words of 10 characters
    int i,j,cnt;
    char delimiter;
    int position;
    //char str_pos;

    printf("Enter a string: ");
    fgets(str, 512, stdin);
    
	printf("Enter a delimiter: ");
	scanf("%c", &delimiter);

	printf("Entar a position:  " );
	scanf("%d", &position);

    j=0; cnt=0;
    for(i=0;i<=(strlen(str));i++)
    {
        // if space or NULL found, assign NULL into splitStrings[cnt]
        if(str[i] == delimiter||str[i]=='\0')
        {
            splitStrings[cnt][j]='\0';
            cnt++;  //for next word
            j=0;    //for next word, init index to 0
        }
        else
        {
            splitStrings[cnt][j]=str[i];
            j++;
        }
      
    }
    printf("\nOriginal String is: %s",str);
    printf("\nStrings (words) after split by space:\n");
    for(i=0;i < cnt;i++)
        printf("%s\n",splitStrings[i]);

    printf("%s\n", splitStrings[position]);

    char str2[32];
    char res[32];
    char temp[32];

    strcpy(str2, splitStrings[position]);
    int n=0;
    for (i =0; i < strlen(str2); i++) {
        if(isalpha(str2[i]) == 0) {
           res[n]=str2[i];
           n++;
        }
    }
    res[strlen(res)]= '\0';

    printf("number only: %s\n", res);
    
    return 0;
}
